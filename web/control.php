<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 
 //librerias requeridas
include './lib/sql.5.5.php'; 
include './lib/usuarios.php';
include './lib/pago.php';
include './lib/ponencia.php';
include './lib/seguridad.php';
include './lib/mail.php';

session_start();
//inicializar variables
$pagoMje = "";
$ponenciaMje = "";
$mje = "";
$mail = "";
if (!isset($_SESSION["importe"])) 
 {
     $_SESSION["importe"] = ""; // si        
     $_SESSION["efectivo"] = "";// si
     $_SESSION["deposito"] = "";// si
     $_SESSION["tarjeta"] = "";// si
     $_SESSION["idUsuario"] = "";
     $_SESSION["personas"] = "";// si
     $_SESSION["pago"] = 0; // idPago
     $_SESSION["mje"] = "";
 }

	//echo "datos:".$_SESSION["importe"]." ".$_SESSION["efectivo"]." ".$_SESSION["deposito"]." ".$_SESSION["tarjeta"]." ".$_SESSION["idUsuario"]." ".$_SESSION["personas"]." idpago:".$_SESSION["pago"];

//obtener datos
if(isset($_POST['enviar']) And $_SESSION["tmptxt"] == $_POST['captcha'])
{
        $mail = "Datos del usuario: \n";
    $user = new Usuarios;
    //Guardamos en variables los datos enviados
    $user->apellido = $_POST['apellido'];
        $mail .= "Apellido: ".$user->apellido."\n";
    $user->nombre = $_POST['nombre'];
        $mail .= "Nombre: ".$user->nombre."\n";
    $user->tipoDoc = $_POST['tipoDoc'];
        $mail .= "Tipo Doc: ".$user->tipoDoc."\n";
    $user->nroDoc = $_POST['nroDoc'];
        $mail .= "Nro Doc: ".$user->nroDoc."\n";
    $user->pais = $_POST['pais'];
        $mail .= "Pais:".$user->pais."\n";
	if ($user->pais == "Argentina")
	{
		$user->provincia = $_POST['provincia'];
	}
	 else
    {
		$user->provincia = 0;
	}    
        $mail .= "Provincia : ".$user->provincia."\n";
    $user->ciudad = $_POST['ciudad'];
        $mail .= "Ciudad : ".$user->ciudad."\n";
    $user->direccion = $_POST['direccion'];
        $mail .= "Direccion : ".$user->direccion."\n";
	$user->cp = $_POST['cp'];
        $mail .= "Codigo Postal: ".$user->cp."\n";
	$user->telefono = $_POST['telefono'];
        $mail .= "Telefono: ".$user->telefono."\n";
	$user->correo = $_POST['correo'];
        $mail .= "Correo: ".$user->correo."\n";
	$user->categoria = $_POST['categoria'];
        $mail .= "Categoria: ".$user->categoria."\n";
	$user->leng = $_POST['leng'];
        $mail .= "Lenguaje: ".$user->leng."\n";
	$socio = $_POST['socioT']; // si es socio guardar la entidad	
	   
	if ($socio == "si")
    {
		$user->socio = $_POST['socio'];
	} 
	else
    {
		$user->socio = 0;
	}    
		$mail .= "Socio: ".$user->socio."\n";
		
	$seg = new Seguridad;
    $user->codigo = $seg->generaCadena(80); 
    
    //corregir por aqui
    if (!isset($_SESSION["pago"]))
    {        
        if (!isset($_POST['socio'])) 
        {
           $user->idPago = 0 ; 
        } 
        else 
        {
            $user->idPago = $_POST['socio'] ; 
        }
        
    }
     else
    {        
        $user->idPago = $_SESSION["pago"] ;                
    }
    
    /*echo "<br/> idpago: ".$user->idPago.
        "<br/> Seccion: ".$_SESSION["pago"].
        "<br/> Post:".$_POST["pago"];*/
    
	
    //registrar usuario
    if ($user->Nuevo())
    {    
      //echo "Pago post: ".$_POST['pago']." Seccion pago: ".$_SESSION["pago"];
	  if(($_POST['pago'] != "") Or  ($_SESSION["pago"] == 0))// pago individual
	  {
	  	//registro de medios de pago
      	$pagos = new Pagos; 	  	  
	  	$pagos->forma = $_POST['formasP'];
            $mail .= "Forma de Pago: ".$pagos->forma."\n";
	  	switch ($pagos->forma) 
	  	{
		  case 'efectivo':
			  $pagos->dato = "";
              $_SESSION["efectivo"] = "selected=\"selected\"";
		  break;
		  
		  case 'deposito':
			  $pagos->dato = $_POST['transaccion'];
              $_SESSION["deposito"] = "selected=\"selected\"";
		  break;
			  
		  case 'tarjeta':
			  $pagos->dato = $_POST['tarjetaP'];
              $_SESSION["tarjeta"] = "selected=\"selected\"";
		  break;	  
	  	}        
         
          $pagos->importe = $_POST['importe'];
                $mail .= "Importe: ".$pagos->importe."\n";
          $_SESSION["importe"] = $_POST['importe'];
		     
		  if($pagos->nuevo())
		  {	  			
				$pagos->campos = "idPago";
				$pagos->condicion = "forma='".$pagos->forma."' AND dato='".$pagos->dato.
								"' AND importe=".$pagos->importe." ORDER BY idPago";
				$aux = $pagos->consultaSQLbasicaRow();
                
				$_SESSION["pago"] = $aux["idPago"];
					$mail .= "IdPago: ".$aux["idPago"]."\n";
				$pagos->campos = "forma,dato,importe";   
				
				$pagoMje= "Pago registrado correctamente";                    
	  	  }
	  	  else
	  	 {
			$pagoMje= "Pago no registrado, por favor envie un mail a --";  
	  	 }	  		
	  	
	  	   
	  	   //registro del pago en la tabla usuarios
	  	 $user->campos = "idUsuario";
         $user->condicion = "doc='".$user->nroDoc."' AND tipoDoc='".$user->tipoDoc
                         ."' ORDER BY idUsuario DESC"; 
         $id = $user->consultaSQLbasicaRow(); 
         $_SESSION["idUsuario"] = $id["idUsuario"];
         $mje = "Usuario ".$id["idUsuario"]." registrado correctamente";          
         $user->campos = "idPago"; 
         $user->valores = $_SESSION["pago"];
         $user->condicion = "idUsuario=".$id["idUsuario"];
         $user->modificarSQL();
		 }
		 //pago grupal
		 
         if ((!isset($_POST['incripcionG'])) Or ($_POST['incripcionG']=="si")) 
         {
         	$_POST['incripcionG']="si";
         	if(!isset($_POST['cant']))
			{
				$_SESSION["personas"] = $_SESSION["personas"]- 1;
			}
			else
			{
				$_SESSION["personas"] = $_POST['cant'];
			}                                  	 
         }   
       	else
       	{       	    
        	  session_destroy(); // si no es grupal, no sirve conservar la session 
       	}   
       
      
	   $ponecias = new Ponencia;
	   $ponecias->idUser = $_SESSION["idUsuario"];
       
	   for ($i=1; $i <3 ; $i++) 
	   {
	   		$ponecias->ponencia = $_POST['ponencia'.$i];
	   			$mail .= "Ponencia: ".$ponecias->ponencia."\n";
                
	   		$ponecias->eje = $_POST['ejes'.$i];	   
	   			$mail .= "Eje: ".$ponecias->eje."\n";	
                
            $ponecias->contendido = $_POST['contenidos'.$i];
                $mail .= "Contenido: ".$ponecias->contendido."\n";
            
			$ponecias->archivo = $_FILES['ponencia'.$i."F"]["name"];
				$mail .= "Archivo: ".$ponecias->archivo."\n";            
            
                
			$arch = $_FILES['ponencia'.$i."F"]["name"];   
                     
            $dir = "upload/arch".$_SESSION["idUsuario"]."_".$arch;
            $tipos = array("application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            if(in_array($_FILES['ponencia'.$i."F"]['type'],$tipos))
            {          
                if(copy($_FILES['ponencia'.$i."F"]['tmp_name'], $dir))
                {
                      $ponenciaMje .= "<br/><p class='text-success'>El archivo ".
                                    basename( $_FILES['ponencia'.$i."F"]['name']).
                                    " ha sido subido </p>";
                      if (($ponecias->ponencia != "") And
                            ($ponecias->archivo) != "")
                        {               
                            if ($ponecias->nuevo())
                            {
                                $ponenciaMje .= "<p>Ponencia:".$_POST['ponencia'.$i].
                                               " cargada correctamente </p>";       
                            }
                            else
                            {
                            	if($ponecias->ponencia =="")
                            	{                            	
									$ponenciaMje .= "<p class='text-danger'>Ponencia:".$_POST['ponencia'.$i].
                                               " no cargada correctamente </p>
                                            <p>Intentelo de nuevo por favor</p>";
								}
                            
                            }               
                        }
                }
                else
                {
                    $ponenciaMje .= "<br/><p class='text-danger'>
                                        ¡Ha ocurrido un error, trate de nuevo!
                                      </p>";
                }
             }
            else 
            {
                $ponenciaMje .= "<p class='text-danger'>Archivo: ".
                				basename( $_FILES['ponencia'.$i."F"]['name']).
                                " en formato no permitido no permitido</p>";   
            }            
			   
	   }
    } 
    else
    {
      $mje = "<p class='text-danger'>Fracaso en el registro de usuario</p>";  
    }
	
	
}
else
{
	$_SESSION["mje"] = "Fallo en la carga de datos. Por favor completelos nuevamente";
    header('Location: formulario.php');
}

 if ($_REQUEST['leng']=="port")
 {
     $data = array( "titulo" => "Ficha de inscrição");
 }
 else
 {
     $data = array( "titulo" => "Formulario de Inscripción");
 }
?>

<!doctype html>
<html lang="es">
<head>
    <title><?php echo $data["titulo"]?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <hr />
        <div class="text-center">
            <h1><?php echo $data["titulo"]?></h1>
        </div>
        <?php echo "<p>".$mje."</p> <br/>"
                   ."<p>".$pagoMje."</p>".
                        $ponenciaMje
                   ."
                     <p>Dudas o consulas puede hacerlas al siguiente mail: 
                     <a href='mailto:congreso@ulapsi.org'>congreso@ulapsi.org</a></p><br/>";
					 
                     //echo " Personas: ".$_SESSION["personas"]." inscripcion Grupal: ".$_POST['incripcionG'];
                     if (($_SESSION["personas"] > 0) And ($_POST['incripcionG']=="si")) 
                     {
                         echo "<p>Ud realizo un registro grupal, aun quedan ".
                         $_SESSION["personas"]." personas a inscribir, si desea
                         hacerlo, puede hacer <a href='formulario.php?gpl=1'> click aqui</a> 
                         </p>";
                     }
                      else
                     {
                         echo "<p> Si desea realizar una nueva inscripcion puede
                               hacer <a href='formulario.php'>clic aqui</a></p>";
                     }
                    /*$email = new email; $email->user = $id["idUsuario"];
                     if ($email->comprobar("d=".$seg->encriptar($user->nroDoc)
                                          ."&n=".$seg->encriptar($user->nombre)
                                          ."&a=".$seg->encriptar($user->apellido)))
                     {                   
                         echo "Email Enviado Correctamente";
                     }
                      else
                     {
                         echo "Error en el envio de mail";
                     }*/
                          
                     ?>
    </div>
</body>
</html>


