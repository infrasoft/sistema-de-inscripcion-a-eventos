/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

//Control para los pagos con tarjetas o deposito bancario
function control()
{
	
	var  lista = document.getElementById('formasP');;
	var valor = lista.options[lista.selectedIndex].value;
	
	  
    switch(valor)// tengo que controlar aqui
    {
    	case "deposito":
    		document.getElementById("transaccion").className = "form-group";
    		document.getElementById("pagosTarjetas").className = "form-group hidden";    		
    	break;
    	
    	case "tarjeta":
    	   document.getElementById("transaccion").className = "form-group hidden";
    		document.getElementById("pagosTarjetas").className = "form-group";
    	break;
    	
    	case "efectivo":
    	   document.getElementById("pagosTarjetas").className = "form-group hidden";
    	   document.getElementById("transaccion").className = "form-group hidden";
       break; 
    }
}

function controlSocio()
{   
   
    if (document.getElementById('socioY').checked)
    {
      document.getElementById("entidadSocio").className = "form-group";
    }
    
    if (document.getElementById('socioN').checked)
    {
      document.getElementById("entidadSocio").className = "form-group hidden";
    }
}

function controlGrupo()
{
    if (document.getElementById('incripcionGrupalY').checked)
    {
      document.getElementById("cantPersonas").className = "form-group";      
    }
    
    if (document.getElementById('incripcionGrupalN').checked)
    {
      document.getElementById("cantPersonas").className = "form-group hidden";
    }
}

//sirve para actualizar captcha
function actucap()
{   
    var obj = document.getElementById("imgcaptcha");
        num = 0;
    if (!obj)
    {
        obj = window.document.all.captcha;
    }  
    num ++;   
    if (obj)
    {
      obj.src = "./lib/captcha.php?" + Math.random() + num;    
    }  
}

