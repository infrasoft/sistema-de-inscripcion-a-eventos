﻿<?php
/*************************************
 *****Sistema Escuela Zorrilla********
 *Sistema de manejo y administracion 
 *de Netbook***********************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
*Sitio Web: http://www.infrasoft.com.ar 
****************************************/ 

// librerias requeridas
//sql.php  Session activa

// clase creada para la seguridad del sistema
class Seguridad extends SQL{
	//instacias
	public $user="";
	public $pass="";
	public $verif="";
	public $verifica="";
	public $logueado=FALSE;
	public $intentos =0;	
	public $tiempo=3000;
	public $hora = "";
	
	
	public function  __construct()
	{
		$this->tabla = "user";
        $this->campos = "usuario,pass,verif";        	
	}
	
	//verifica si el login es correcto
	public function login()
	{
        $this->pass = $this->encriptar($this->pass);
		$this->condicion ="usuario='".$this->user."' AND pass='".$this->pass."'";	
        $row = $this->consultaSQLbasicaRow();
        //echo "Password: ".$this->pass;
        if(is_array($row))
        {            
            $this->user = $row["usuario"];
			$_SESSION["usuario"] = $this->encriptar($row["usuario"]);
			
			$this->pass = $row["pass"];
			$_SESSION["pass"] = $this->pass;
			
			$this->verif = $this->encriptar($row["verif"]);
			$_SESSION["verif"] = $this->verif;
			
			$this->verifica = $_SESSION["usuario"].
							  $_SESSION["pass"].
							  $_SESSION["verif"];
			$_SESSION["verifica"] = $this->verifica;
			$this->logueado = TRUE;
			$this->hora=time();		
			$_SESSION["hora"]=time();		
            //$this->Imprime();
			//echo "Verificado: ".$this->verifica;
			return 1;
        }
        else
        {
            $this->intentos = $this->intentos + 1;
			header('Location: logout.php');
			return 0;
        }
	}
	
	//verifica la actividad del usuario, si esta o no frente al sistema
	public function verifica() 
	{
		if (!isset($_SESSION["verifica"]))
		{
			$_SESSION["verifica"] = $this->generaCadena(24);
		}
		
		if (	($_SESSION["usuario"].$_SESSION["pass"].$_SESSION["verif"]
				== $_SESSION["verifica"])
				and ($this->hora+$this->tiempo >= time()) 
			)
		{
			$this->hora = time();
			$_SESSION["hora"] = time();
			return 1;
		}
		 else
		{
			//header("Location: logout.php");
			//echo "Salio de la seccion";
			return 0;
		}
	}
	
	// muestra todos los datos del objeto
	public function Imprime()
	{
		echo "Usuario: ".$this->user." ".$_SESSION["usuario"]."<br/>".
			 "Password: ".$this->pass." ".$_SESSION["pass"]."<br/>".
			 "Verif: ".$this->verif." ".$_SESSION["verif"]."<br/>".
			 "Verificacion: ".$this->verifica." ".$_SESSION["verifica"]."<br/>".
			 "Logueado: ".$this->logueado."<br/>".
			 "Tiempo:".$_SESSION["hora"];
	}		 
	//verifica que los datos ingresados no sea maliciosos
	public function texto($var)
	{
		return filter_var($var,FILTER_SANITIZE_EMAIL);
	}	
	
	// verifica que el ingreso solo sean numeros
	public function numeros()
	{
		
	}
	
	// genera un vector de numeros aleatorios, sin repetir 
    function generaAleatios($cantidad, $tope)
    {
        $aux[] = null;
        $num = rand(1,$tope);
        for ($i=0; $i < $cantidad; $i++)
        {
            if(in_array($num, $aux))
            {
                $i--;
            }
            else
            {
               $aux[$i] =  $num;
            }
            $num = rand(1,$tope);
        }               
        return $aux;        
    }
    
     //Genera una cadena aleatoria para confirmar seccion
    public function generaCadena($num)
    {
        $cad = '';
        $codigo = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($i=0; $i< $num;$i++)
        {
			$pos = rand(1,55);            
            $cad .=  $codigo[$pos];
        }        
        return $cad;
    }
	
	//encripta los numeros de una manera unica
	function encriptar($var)
	{
		$n = strlen($var);
		$codigo = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';		
		$encrip = sha1(crypt($var,$codigo));
		return $encrip;
	}
}

$_SESSION['seguridad'] = new Seguridad;
//session_start();
/*
if(!is_object($_SESSION['seguridad']))
{
	$_SESSION['seguridad'] = new Seguridad;
    header("Location: login.php");
}*/

?>