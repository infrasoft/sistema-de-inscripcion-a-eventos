<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 //librerias requeridas: sql
 
 /**
  *  Clase para las ponencias de los usuarios
  */
 class Ponencia extends SQL 
 {
     public $idPonencia = 0; // no se ocupa, pero por si las dudas lo dejo
     public $idUser = 0;
     public $ponencia = "";
     public $eje = 0;
     public $contendido = 0;
     public $archivo = "";
     function __construct()
	 {
         $this->tabla = "ponencia";
		 $this->campos = "idUser,ponencia,eje,contendido,archivo";
     }
	 
	 //registra una nueva ponencia
	 public function nuevo()
	 {
		 $this->valores = $this->idUser.",'".$this->ponencia."',".
		                  $this->eje.",".$this->contendido.",'".$this->archivo."'";
		 return $this->insertaSQL();
	 }
	 
	 //realiza la modificacion de una ponencia
     public function modifica()
     {
         $this->campos = "idUser,ponencia,eje,contendido,archivo";
         $this->valores = $this->idUser.",'".$this->ponencia."',".
                          $this->eje.",".$this->contendido.",'".$this->archivo."'";
         $this->condicion = "idPonencia=".$this->idPonencia;
         return $this->modificarSQL();
     }
	 
	 //realiza la consulta de una ponencia
	 public function consulta()
	 {
		 $this->campos = "idUser,ponencia,eje,contendido,archivo";
         $this->valores = $this->idUser.",'".$this->ponencia."',".
                          $this->eje.",".$this->contendido.",'".$this->archivo."'";
         $this->condicion = "idPonencia=".$this->idPonencia;
		 return $this->consultaSQLbasicaRow() ;
	 }
	 
	 //elimina un elemento de la base de datos
	 public function eliminar()
	 {
		 $this->condicion = "idPonencia=".$this->idPonencia;
		 return $this->BorraSQL();
	 }
 }
 
 ?>