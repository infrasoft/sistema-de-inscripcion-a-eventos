<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 //librerias requeridas: sql
 
 /**
  *  Clase para el manejo de usuarios
  */
 class Usuarios extends SQL
 {
 	public $apellido = "";
	public $nombre = "";
	public $tipoDoc = 0; 
	public $nroDoc = ""; 
	public $pais = ""; 
	public $provincia = 0; 
	public $ciudad = "";     
	public $cp = ""; 
	public $direccion = ""; 
	public $telefono = ""; 
	public $correo = "";
	public $categoria = 0;
    public $leng = "esp"; //español por defaul
    public $socio = 0; //no es socio por defaul
    public $verificado = "no";
    public $presente = "no"; 
    public $emitido  = "no";   
    public $codigo = "";
    public $idPago = 0;
     //constructores
     function __construct()
	 {
	    $this->tabla = "usuarios";
     	$this->campos = "apellido,nombre,tipoDoc,doc,pais,provincia,cp,direccion"
     	                .",telefono,email,categoria,leng,socio,verificado,"
     	                ."presente,emitido,codigo,idPago";    
     }
	 
	 //nuevo usuario
	 public function Nuevo()
	 {
		 $this->valores = "'".$this->apellido."','".$this->nombre."','".$this->tipoDoc
                          ."','".$this->nroDoc."','".$this->pais."',".$this->provincia  
                          .",'".$this->cp."','".$this->direccion."','".$this->telefono
                          ."','".$this->correo."','".$this->categoria."','".$this->leng
                          ."',".$this->socio.",'".$this->verificado."','".$this->presente
                          ."','".$this->emitido."','".$this->codigo."',".$this->idPago;
		 return $this->insertaSQL();
	 }
     
     //consulta los datos de un usuario
     public function consulta($id)
     {
         $this->condicion = "idUsuario=".$id;
         return $this->consultaSQLbasicaRow();
     }
     
     //modifica datos del usuario
     public function modifica($id) //modificar
     {
     	$this->condicion = "idUsuario=".$id;
		$this->campos = "apellido,nombre,tipoDoc,doc,pais,provincia,cp,direccion"
     	                .",telefono,email,categoria,leng,socio,verificado";
         $this->valores = "'".$this->apellido."','".$this->nombre."','".$this->tipoDoc
                          ."','".$this->nroDoc."','".$this->pais."',".$this->provincia  
                          .",'".$this->cp."','".$this->direccion."','".$this->telefono
                          ."','".$this->correo."','".$this->categoria."','".$this->leng
                          ."',".$this->socio.",'".$this->verificado."'";
         return $this->modificarSQL();
     }
	 
	 //elimina un usuario
     public function elimina($id)
     {
         $this->condicion = "idUsuario=".$id;
		 return $this->BorraSQL();
     }
 }
 
?>