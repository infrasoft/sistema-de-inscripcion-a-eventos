<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 //librerias requeridas: sql
 
 /**
  * Clase para el manejo de socios
  */
 class Socios extends SQL 
 {
     public $idSocio = 0;
	 public $entidad = "";
	 
     function __construct()
	 {
         $this->tabla = "socio";
		 $this->campos = "id,entidad";
     }
	 
	 public function selectSocio()
	 {
		 $this->estilo = "form-control";
		 $this->id = "id";
		 $this->muestra = "entidad";
		 return $this->selectTabla(); 
	 }
 }
     
?>