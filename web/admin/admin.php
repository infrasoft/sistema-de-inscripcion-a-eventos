<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
if(!isset($_SESSION["verif"]))
{    
    header("Location: login.php");
} 
 //librerias requeridas
include '../lib/sql.5.5.php';
include '../lib/usuarios.php';
include '../lib/seguridad.php'; 

 //inicializar variables 
 
 $_SESSION['seguridad']->verifica();
 
 $user = new Usuarios;
 $mje="";
 
 if (isset($_REQUEST["campo"]))
 {
     $user->condicion = $_REQUEST["campo"].
                        " LIKE '%".
                        $_REQUEST["buscar"]."%'";
     $mje = "Datos filtrados de la busqueda del dato <b>".
                $_REQUEST["buscar"]."</b> del campo <b>".
                $_REQUEST["campo"]."</b>";
 } 
 
 $user->campos = "idUsuario,apellido,nombre,tipoDoc,doc,pais,provincia,cp,direccion"
     	                .",telefono,email,categoria,leng,socio,verificado,"
     	                ."presente,emitido,codigo,idPago";
 $consulta = $user->consulSQLbasica();
 
 ?>
 <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>        
        <div class="text-right bold">
        	<a href="logout.php"  class="text-danger"><b>Desconectarse</b></a>
        </div>
        <hr />

<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="admin.php">Inscriptos</a></li>
      <li><a href="ponencia.php">Ponencias</a></li>
      <li><a href="pagos.php">Pagos</a></li>
      <li><a href="ejes.php">Ejes</a></li>
      <li><a href="contenidos.php">Contenidos</a></li>
    </ul>
 
    <form class="navbar-form navbar-left" role="search" action="admin.php">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar" name="buscar">
        <select class="form-control" name="campo">
        	<option value="apellido">apellido</option>
        	<option value="nombre">nombre</option>
        	<option value="doc">documento</option>
        	<option value="telefono">telefono</option>
        	<option value="email">email</option>
        	<option value="categoria">categoria</option>
        </select>
      </div>
      <button type="submit" class="btn btn-default">Buscar</button>
    </form>   
  </div>
  
</nav>

	<div class="text-center">
		<h1> Lista de usuarios</h1>
	</div>
	
	<p class="text-primary"><?php echo $mje; ?></p>
	
	<div class="table-responsive">
	<table class="table table-striped">
		<tr>
			<td><b>Id</b></td>
			<td><b>Apellido</b></td>
			<td><b>Nombre</b></td>
			<td><b>Documento</b></td>
			<td><b>Telefono</b></td>
			<td><b>Email</b></td>
			<td><b>IdPago</b></td>
			<td><b>Categoria</b></td>
			<td><b>Verificado</b></td>
			<td><b>Presente</b></td>
			<td><b>Emitido</b></td>
			<td><b>Opciones</b></td>
		</tr>
		<?php
		while($row = $consulta->fetch_assoc())
		{
 		   echo "<tr>
 		   			<td>".$row['idUsuario'] .
 		   			'</td>
 		   		   <td>'.$row['apellido'].
 		   		   '</td>
 		   		   <td>'.$row['nombre'].
				   "</td>
				   <td>".$row['doc'].
				   "</td>
				   <td>".$row['telefono'].
				   "</td>
				   <td>".$row['email'].
				   "</td>
				   <td>".$row['idPago'].	
				   "</td>
				   <td>".$row['categoria'].
				   "</td>
				   <td>".$row['verificado'].
				   "</td>
				   <td>".$row['presente'].
				   "</td>
				   <td>".$row['emitido'].			   		
				   "</td>
				   <td>
				   	
				   	<a href='usuarios.php?id=".$row['idUsuario']."' title='Modificar datos'>
				   	    <span class='glyphicon glyphicon-pushpin'> </span>
				   	</a> <!-- modificar datos -->
				   	
				   	<a href='ponencia.php?buscar=".$row['idUsuario']."&campo=idUser' title='Filtrado de Ponencias del usuario'>
				   	    <span class='glyphicon glyphicon-hdd'> </span>
				   	</a> <!-- Ponencias -->
				   	
				   	<a href='ponencia.php?op=agregar&id=".$row['idUsuario']."' title='Subir una ponencia'>
				   	    <span class='glyphicon glyphicon-floppy-open'> </span>
				   	</a> <!-- Subir ponencias-->
				   	
				   	<a href='pagos.php?buscar=".$row['idPago']."&campo=idPago' title='Pagos del usuario'>
				   	    <span class='glyphicon glyphicon-usd'> </span>
				   	</a> <!-- Pagos -->
				   	
					<a href='usuarios.php?id=".$row['idUsuario']."&op=asist' title='Verifica la asistencia'>
				   	    <span class='glyphicon glyphicon-hand-up'> </span>
				   	</a> <!-- Verifica la asistencia-->
				   	
				   	<a href='pdf.php?i=".$row['idUsuario']."' title='Imprimir certificados' target='_blank'>
				   	    <span class='glyphicon glyphicon-print'> </span>
				   	</a> <!-- Imprimir -->
				   	
					<a href='pdf.php?i=".$row['idUsuario']."&o=2' title='Imprimir certificados en blanco' target='_blank'>
				   	    <span class='glyphicon glyphicon-print'> </span>
				   	</a> <!-- Imprimir en blanco-->
				   	
				   	<a href='pdf.php?i=".$row['idUsuario']."&o=1' title='Imprimir certificado con firma' target='_blank'>
				   	    <span class='glyphicon glyphicon-envelope'> </span>
				   	</a> <!-- Enviar por correo -->
				   	
					<a href='eliminar.php?id=".$row['idUsuario']."' title='Eliminar un usuario'>
				   	    <span class='glyphicon glyphicon-remove'> </span>
				   	</a> <!-- Eliminar a un usuario -->
				   </td>
				 </tr>";
		} 
		?>
	</table>        
    </div>
    
    <div class="text-center">
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>
