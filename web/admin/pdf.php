<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
include '../lib/sql.5.5.php';
include '../lib/fpdf.php';
include '../lib/usuarios.php';
include '../lib/ponencia.php';

//variables
if (!isset($_REQUEST["i"]))
{
    $i=0;
    header('Location: logout.php');//cuestion de seguridad
}
else
{
    $i = $_REQUEST["i"];
    if (!isset($_REQUEST["o"]))
    {
        $o = 0;        
    }
    else
    {
        $o = $_REQUEST["o"];
    }
}

//falta crear las medidas de seguridad

define('FPDF_FONTPATH','../lib/font/');

$pdf=new FPDF("L");
$pdf->AddPage();
//$pdf->SetFont('Arial','B',16);
switch ($_REQUEST["o"])
        {
            case 0: // impresion sin firma
                $pdf->Image("../img/usuario.jpg",'0','0','297','210','JPG');
            break;
            
            case 1: // impresion con firma
                $pdf->Image("../img/usuario1.jpg",'0','0','297','210','JPG');
            break;
            
            case 2: // impresion sin fondo
                
            break;         
        }

$pdf->Ln(67);
$pdf->Cell(90,40,'','','','C','');
$pdf->SetFont('Arial','B',20);
$user = new Usuarios;
$row = $user->consulta($i);
$pdf->Cell(270,4,utf8_decode($row['apellido'])." ".utf8_decode($row['nombre'])." ".$row['tipoDoc'].":".$row['doc']); //problemas para mostrar el tipo doc
$pdf->Ln(10);
$pdf->Cell(120,40,'','','','C','');
$pdf->Cell(270,4,$row['categoria']);
$pon = new Ponencia;
$pon->condicion = "idUser=".$i;
$consulta = $pon->consulSQLbasica();
if($consulta != null)
{
    while ($row2 = $consulta->fetch_assoc())
    {
       $pdf->AddPage();
       switch ($_REQUEST["o"])
        {
            case 0: // impresion sin firma
                $pdf->Image("../img/ponencia.jpg",'0','0','297','210','JPG');
            break;
            
            case 1: // impresion con firma
                $pdf->Image("../img/ponencia1.jpg",'0','0','297','210','JPG');
            break;
            
            case 2: // impresion sin fondo
                
            break;         
        }
       
       $pdf->Ln(70);
       $pdf->Cell(90,40,'','','','C','');
       $pdf->SetFont('Arial','B',20);
       $pdf->Cell(270,4,utf8_decode($row['apellido'])." ".utf8_decode($row['nombre'])." ".$row['tipoDoc'].":".$row['doc']);
       $pdf->Ln(20);
       $pdf->Cell(50,10,'','','','C','');
       $pdf->Cell(70,2,utf8_decode($row2['ponencia'])); 
    }    
}

$pdf->Output();

$user->condicion = "idUsuario=". $i;
$user->campos  = "emitido";
$user->valores = "'si'";
$user->modificarSQL();
