<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
 //librerias requeridas
 include '../lib/sql.5.5.php'; 
 include '../lib/seguridad.php';

  
 //inicializar variables 
 
 if (!isset($_REQUEST["id"]))
 {
     $idUsuario = 0;
	 $consulta = null;
     $menu = ""; 
     $_SESSION['seguridad']->verifica(); 
 }
  else
 {
     $id = $_REQUEST["id"];
     include '../lib/usuarios.php';
	 $user= new Usuarios;
	 $consulta = $user->consulta($id); 
	 if (!isset($_REQUEST["d"]))
	 {
		 $_REQUEST["d"] = "";
		 $_REQUEST["n"] = "";
		 $_REQUEST["a"] = ""; 
	 }
	 
	 
	 $objaux = new Seguridad;
	/*echo "d: ".$objaux->encriptar($consulta["doc"])."<br/>"
		 ."n: ".$objaux->encriptar($consulta["nombre"])."<br/>"
		 ."a: ".$objaux->encriptar($consulta["apellido"])."<br/>";*/
	 if (($_REQUEST["d"].$_REQUEST["n"].$_REQUEST["a"] ==
	 									 $objaux->encriptar($consulta["doc"])
	 									.$objaux->encriptar($consulta["nombre"])
	 									.$objaux->encriptar($consulta["apellido"]))
		 And ($consulta["verificado"] ==0))
	 { // esta seccion es para la corroboracion de usuarios
		$menu = ""; 
	 }
	  else 
	 {// esta seccion es para los administradores	
	 
	 	 
	 	if(!isset($_SESSION["verif"]))
		{
			header("Location: login.php");
		} 
	  	 	
	 	$_SESSION['seguridad']->verifica();  //aqui tengo que ver
	 
	 
	  
	 	$menu = "<div class='text-right bold'>
            <a href='logout.php'  class='text-danger'><b>Desconectarse</b></a>
        </div>
	 	<nav class='navbar navbar-default' role='navigation'>
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class='navbar-header'>
    <button type='button' class='navbar-toggle' data-toggle='collapse'
            data-target='.navbar-ex1-collapse'>
      <span class='sr-only'>Desplegar navegación</span>
      <span class='icon-bar'></span>
      <span class='icon-bar'></span>
      <span class='icon-bar'></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class='collapse navbar-collapse navbar-ex1-collapse'>
    <ul class='nav navbar-nav'>
      <li class='active'><a href='admin.php'>Inscriptos</a></li>
      <li><a href='ponencia.php'>Ponencias</a></li>
      <li><a href='pagos.php'>Pagos</a></li>
      <li><a href='ejes.php'>Ejes</a></li>
      <li><a href='contenidos.php'>Contenidos</a></li>
    </ul>
 
    <form class='navbar-form navbar-left' role='search' action='admin.php'>
      <div class='form-group'>
        <input type='text' class='form-control' placeholder='Buscar' name='buscar'>
        <select class='form-control' name='campo'>
        	<option value='apellido'>apellido</option>
        	<option value='nombre'>nombre</option>
        	<option value='doc'>documento</option>
        	<option value='telefono'>telefono</option>
        	<option value='email'>email</option>
        	<option value='categoria'>categoria</option>
        </select>
      </div>
      <button type='submit' class='btn btn-default'>Buscar</button>
    </form>   
  </div>
  
</nav>";
		
	 }
	 
 }
 
 if (isset($_REQUEST['leng'])) //aqui se seleccionara el idioma del participante
 {//portugues
 	$leng = "port";
     $data = array( "nombre"=>"Nome  (*)",
 				"apellido" => "Sobrenome (*) ",
                "tipoDoc" =>"Tipo de documento: ",
                "nroDoc" => "Número do Documento (*) ",
                "pais" => "Pais ",
                "provincia" => "Provincia",
                "ciudad" => "Cidade (*) ",
                "direccion" => "Direccion (*) ",
                "cp" => "Código postal (*) ",
                "telefono" => "Telefone (*) ",
                "correo" => "e-mail (*) ",
                "socio" => "Associado ",
                "formasP" => "Método de pago ",
                	"efectivo" => "Espécie ",
                	"deposito"=> "Depósito bancário",
                	"tarjeta" => "Cartão Credito  ",
                "transaccion" => "Nr. Transação ",
                "tarjetaP" => "Cantidad de Cuotas",
                "importe" => "Valor (*)",
                "incripcionG" => "inscripción Grupal ",
                "cant" => "Número de pessoas para se registrar",
                "categoria" => "Categoria ",
                	"autor"=> "Autor",
                	"coautor" => "Co-Autor",
                	"participante" => "Participante",
                	"conferencista" => "Conferencista",
                	"estudiante" => "Estudante",
                	"expositor" => "Expositor Convitado",
                "entidad" => "Entidad ",
                "ponencia1" => "Trabalho 1 ",
                "ponencia2" => "Trabalho 2 ",
                "ponencia3" => "Trabalho 3 ",
                "mensaje" =>"Por favor preencha o seguinte formulário para inscrever-se no: VI Congreso ULAPSI.
								Se tiver qualquer duvida ou comentário por favor escreva ao  congreso@ulapsi.org",
                "datosObli" => "Algunos campos filho obligatorios (*):",
                "idioma"=>"Cambio de idioma",
				"titulo" => "Ficha de inscrição",
				"idioma" => "Idioma");
 } 
 else
 {		//español
 	 $leng = "esp";
     $data = array( "nombre"=>"Nombre: (*) ",
 				"apellido" => "Apellido (*) ",
                "tipoDoc" =>"Tipo de documento: ",
                "nroDoc" => "Numero de Documento (*) ",
                "pais" => "Pais ",
                "provincia" => "Provincia ",
                "ciudad" => "Ciudad (*) ",
                "direccion" => "Direccion (*) ",
                "cp" => "Codigo Postal (*) ",
                "telefono" => "Telefono (*) ",
                "correo" => "Correo Electonico (*) ",
                "socio" => "Socio ",
                "formasP" => "Formas de Pago ",
                	"efectivo" => "Efectivo",
                	"deposito"=> "Deposito Bancario",
                	"tarjeta" => "Tarjetas de credito",
                "transaccion" => "Nro de Transaccion ",
                "tarjetaP" => "Cantidad de Cuotas ",
                "importe" => "Importe (*) ",
                "incripcionG" => "Inscripcion Grupal ",
                "cant" => "Cantidad de personas a inscribir",
                "categoria" => "Categoria ",
                	"autor"=> "Autor",
                	"coautor" => "Co-Autor",
                	"participante" => "Participante",
                	"conferencista" => "Conferencista",
                	"estudiante" => "Estudiante",
                	"expositor" => "Expositor Invitado",
                "entidad" => "Entidad ",
                "ponencia1" => "Ponencia 1  ",
                "ponencia2" => "Ponencia 2 ",
                "ponencia3" => "Ponencia 3 ",
                "mensaje" =>"Por favor complete el siguiente formulario de inscripcion para el VI Congreso ULAPSI.
                			 Dudas o consultas puede hacerlo al siguiente mail congreso@ulapsi.org",
                "datosObli" => "Algunos campos son obligatorios (*):",
                "idioma"=>"Cambio de idioma",
				"titulo" => "Formulario de Inscripción",
				"idioma" => "Idioma");				
	}
			
if (!isset($_REQUEST['op']))
{
	$op = "form";	
}
 else
 {
	$op = $_REQUEST['op']; 
}

include '../lib/socios.php';
$socios = new Socios();
$socios->predeterminado = $consulta["socio"];
 
 ?>
  <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
    	<div class="text-center">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        </div>
        <hr />
        
        
        <?php
        	echo $menu;
        	switch ($op)
			 {
				case 'form':
                    $port = "";$esp= "";
                    if($consulta["leng"]=="esp")
                    {
                        $esp= " selected=\"selected\"";
                    }
                    else
                    {
                       $port = " selected=\"selected\"";
                    }
                    
					echo "<h1>Modificar Datos</h1>
    <form class='form-inline' role='form' action='usuarios.php' 
	  method='post'>
	  	<div class='form-group'>    		
    		<input type='hidden' value='".$id."' class='form-control' name='id'/>
    		<div class='form-group'>
    		<label>".$data["apellido"]."</label>
    		<input type='text' value='".$consulta["apellido"]."' class='form-control' name='apellido'/>
    		
    		<label>".$data["nombre"]."</label>
    		<input type='text' value='".$consulta["nombre"]."' class='form-control' name='nombre'/>
    		
    		<label>".$data["tipoDoc"]."</label>
    		<select class='form-control' name='tipoDoc'>
	            <option>DNI</option>
            	<option>CC</option>
            	<option>CI</option>
            	<option>CIC</option>
            	<option>CIE</option>
            	<option>LC</option>
	            <option>LE</option>
            	<option>Pasaporte</option>
	            <option>RG</option>
            	<option>VISA</option>
            	<option>Otro</option>
       		</select>
       		</div>
       		
       		<div class='form-group'>
       			<label>".$data["nroDoc"]."</label>
       			<input type='text' value='".$consulta["doc"]."' class='form-control' name='doc'/>
       			
       			<label>".$data["pais"]."</label>
       			<input type='text' value='".$consulta["pais"]."' class='form-control' name='pais'/>
       			
       			<label>".$data["provincia"]."</label>
       			<select class='form-control' name='provincia'>
                	<option value='1' selected='selected'>Buenos Aires</option>
                	<option value='2'>C.A.B.A.</option>
                	<option value='3'>Catamarca</option>
                	<option value='4'>Chaco</option>
                	<option value='5'>Chubut</option>
                	<option value='6'>Cordoba</option>
                	<option value='7'>Corrientes</option>
                	<option value='8'>Entre Rios</option>
                	<option value='9'>Formosa</option>
                	<option value='10'>Jujuy</option>
                	<option value='11'>La Pampa</option>
                	<option value='12'>La Rioja</option>
                	<option value='13'>Mendoza</option>
                	<option value='14'>Misiones</option>
                	<option value='15'>Neuquen</option>
                	<option value='16'>Rio Negro</option>
                	<option value='17'>Salta</option>
                	<option value='18'>San Juan</option>
                	<option value='19'>San Luis</option>
                	<option value='20'>Santa Cruz</option>
                	<option value='21'>Santa Fe 1ra. Circ.</option>
                	<option value='23'>Santiago del Estero</option>
                	<option value='24'>Tierra del Fuego</option>
	                <option value='25'>Tucuman</option>
                	<option value='22'>Santa Fe 2da. Circ</option>
                	<option value='0'>No Especificado</option>
               	</select>
       		</div>	
       		
       		<div class='form-group'>
       			<label>".$data["cp"]."</label>
       			<input type='text' class='form-control' name='cp' value='".$consulta["cp"]."' required/>
       			
       			<label>".$data["direccion"]."</label>
       			<input type='text' class='form-control' name='direccion' value='".$consulta["direccion"]."' required/>
       			
       			<label>".$data["telefono"]."</label>
       			<input type='text' class='form-control' name='telefono' value='".$consulta["telefono"]."' required/>
       		</div>	
       		
       		<div class='form-group'>
       			<label>".$data["correo"]."</label>
       			<input type='email' class='form-control' name='email' value='".$consulta["email"]."' required/>
       			
       			<label>".$data["categoria"]."</label>
       			<select class='form-control' name='categoria'>
                <option value='autor'>".$data['autor']."</option>
                <option value='coautor'>".$data['coautor']."</option>
                <option value='participante'>".$data['participante']."</option>
                <option value='conferencista'>".$data['conferencista']."</option>
                <option value='estudiante'>".$data['estudiante']."</option>
                <option value='expositor'>".$data['expositor']."</option>
            	</select>
            	
				<label>".$data["idioma"]."</label>
       			<select class='form-control' name='leng'>
       				<option value='port' ".$port.">Portugues</option>
       				<option value='esp' ".$esp.">Español</option>
       			</select>  
       		</div>	
       		<div class='form-group'>
                <label>".$data["socio"]."</label>
                ".$socios->selectSocio()."  			
       		
       		</div>
       		<hr />";
       		
       		if($menu =="") //para la actualizacion de datos de usuario
       		{
       		   echo "<input type='hidden' class='form-control' name='d' value='".$_REQUEST["d"]."' >"
                   ."<input type='hidden' class='form-control' name='n' value='".$_REQUEST["n"]."' >"
                   ."<input type='hidden' class='form-control' name='a' value='".$_REQUEST["a"]."' >";
       		}
       	
       	echo"<div class='text-center'>
       			<button type='submit' class='btn btn-info' name='op' id='op' value='mdf'>Modificar</button>
		    	<button type='reset' class='btn btn-default'>Cancelar</button>	
       		</div>
    	</div>
    </form>";
					break;
					
				case 'mdf':
				echo "<h1>Modificar datos del usuario</h1>";
					$user->apellido = $_REQUEST['apellido'];
					$user->nombre = $_REQUEST['nombre'];
					$user->tipoDoc = $_REQUEST['tipoDoc'];
					$user->nroDoc = $_REQUEST['doc'];
					$user->pais = $_REQUEST['pais'];
					$user->provincia = $_REQUEST['provincia'];
					$user->cp = $_REQUEST['cp'];
					$user->direccion = $_REQUEST['direccion'];
					$user->telefono = $_REQUEST['telefono'];
					$user->correo = $_REQUEST['email'];
					$user->categoria = $_REQUEST['categoria'];
					$user->leng = $_REQUEST['leng']; 
					$user->socio = $_REQUEST['socio']; //falta poner el select preseleccionado
					$user->verificado = 1; 
					if ($user->modifica($id))
					 {
						echo "<p class='text-primary'>Datos modificados correctamente</p>";
					}
					  else
				    {
						echo "<p class='text-danger'>Fallos en la modificacion de datos</p>";
					}
					
					break;
					
					//sirve para actualizar la asistencia de los participantes
					case 'asist':
						echo "<h1>Registro de asistencia</h1>";
						$user->campos= "presente";
						$user->valores = 1;
						$user->condicion = "idUsuario=".$id;
						if ($user->modificarSQL())
						 {
							echo "<p class='text-primary'>Asistencia registrada correctamente</p>";
						}
						  else
					    {
							echo "<p class='text-danger'>Fallos en la registracion de asistencia</p>";
						}
						 
					break;
						
					//Envia el email para imprimir los certificados
					case 'cert':
							 include '../lib/mail.php';
							$ml = new email; //sin terminar
							$ml->user = $id;
							if($ml->certificados())
							{
								echo "<p class='text-primary'>mail enviado correctamente</p>";	
							}
							else
							{
								echo "<p class='text-danger'>Fallo en el envio de mail</p>";	
							}
							
					break;
				default:
					
					break;
			} 
         ?>
    	 
    </div>
</body>
</html>