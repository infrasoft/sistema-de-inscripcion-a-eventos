<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
if(!isset($_SESSION["verif"]))
{    
    header("Location: login.php");
} 
 
 //librerias requeridas
 include '../lib/sql.5.5.php';
 include '../lib/ponencia.php';
 include '../lib/seguridad.php';   
  
 //inicializar variables 
 
 $_SESSION['seguridad']->verifica();

  $mje ="";
  $pon= new Ponencia;
  $pon->campos = "idPonencia,".$pon->campos;
 
 if (isset($_REQUEST["campo"])) 
 {
    $pon->condicion = $_REQUEST["campo"].
                        " LIKE '%".
                        $_REQUEST["buscar"]."%'";
      $mje = "Datos filtrados de la busqueda del dato <b>".
                $_REQUEST["buscar"]."</b> del campo <b>".
                $_REQUEST["campo"]."</b>";     
 }
  
 
 	$consulta = $pon->consulSQLbasica();
    
 	if (!isset($_REQUEST["id"]))
   {
      $id = 0; 
      $op="lista";
   }
    else 
  {
     $id = $_REQUEST["id"];
	 $pon->idPonencia = $id;	
	 $row = $pon->consulta(); 
     if (!isset($_REQUEST["op"]))
      {
         $op = "";
     } 
     else 
     {
         $op = $_REQUEST["op"]; 
     }      
  }
 
 
 
 ?>
 <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <div class="text-right bold">
            <a href="logout.php"  class="text-danger"><b>Desconectarse</b></a>
        </div>
        <hr />
        
        

<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li ><a href="admin.php">Inscriptos</a></li>
      <li class="active"><a href="ponencia.php">Ponencias</a></li>
      <li ><a href="pagos.php">Pagos</a></li>
      <li><a href="ejes.php">Ejes</a></li>
      <li><a href="contenidos.php">Contenidos</a></li>     
    </ul>
    
 	<div class="table-responsive">
    <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar" name="buscar">
        <select class="form-control" name="campo">
        	<option value="idPonencia">idPonencia</option>
        	<option value="idUser"> idUser</option>
        	<option value="ponencia">Titulo</option>
        	<option value="eje">eje</option>
        	<option value="contendido">contendido</option>
        	<option value="archivo">archivo</option>
        </select>
      </div>
      <button type="submit" class="btn btn-default">Buscar</button>
    </form>
 	
    </div>
  </div>
</nav>
        <?php 
            switch ($op) 
            {
                case 'lista':
                    echo "<h1> Lista de Ponencias</h1>
                           <p>".$mje."</p>
                     <table class=\"table table-striped\">
		              <tr>
			             <td><b>Id Ponencia</b></td>
			             <td><b>Id Usuario</b></td>
			             <td><b>Titulo</b></td>
			             <td><b>Eje</b></td>
			             <td><b>Contenido</b></td>
			             <td><b>Archivo</b></td>			
			             <td><b>Opciones</b></td>
		              </tr>";  
                     while($row = $consulta->fetch_assoc())
		              {
 		             echo "<tr>
 		   			           <td>".$row['idPonencia'] .
 		   		               '</td>
 		   		                <td>'.$row['idUser'].
				                "</td>
				                <td>".$row['ponencia'].
				                "</td>
				                <td>".$row['eje'].
				                "</td>
				                <td>".$row['contendido'].
				                "</td>
				                <td>".$row['archivo'].				   			   		
				            "</td>
				            <td>
				   	        <a href='../upload/arch".$row['idUser']."_".$row['archivo'].
				   	        "' title='Descarga de Ponencia'>
				   		           <span class='glyphicon glyphicon-download'> </span>
				   	        </a> <!-- Descargar -->
				   	
				   	        <a href='ponencia.php?op=mdf&id=".$row['idPonencia']."' title='Modificar datos'>
				   		       <span class='glyphicon glyphicon-pushpin'> </span>
				   	        </a> <!-- Modificar -->
				   	
				   	        <a href='admin.php?buscar=".$row['idUser'].
				   	        "&campo=idUsuario' title='Usuario asociado a la ponencia'>
                    		  <span class='glyphicon glyphicon-sort'> </span>
                    	   </a>
                    	   
                    	   <a href='ponencia.php?op=eliminar&id=".$row['idPonencia']."' title='Modificar datos'>
				   		       <span class='glyphicon glyphicon-remove'> </span>
				   	        </a> <!-- Modificar -->
				         </td>
				        </tr>";
		              }
		              echo "</table>";
                break;
                
                //modifica los datos de ponencias
                case 'mdf':
					
                    echo "<h1>Modificar datos de ponencia</h1>
                    <form class=\"form-inline\" role=\"form\" action=\"ponencia.php\">
           				<label>Id Ponencia: <i>".$id."</i> </label>
           				<input type=\"hidden\" name=\"id\" value=\"".$id."\" class=\"form-control\"/>
           				<label>idUser</label>
           				<input type=\"number\" name=\"idUser\" value=\"".$row["idUser"]."\" class=\"form-control\"/>
           				<label>Titulo</label>
           				<input type=\"text\" name=\"ponencia\" value=\"".$row["ponencia"]."\" class=\"form-control\"/>
           				<label>Eje</label>
           				<input type=\"number\" name=\"eje\" value=\"".$row["eje"]."\" class=\"form-control\"/>
           				<label>Contenido</label>
           				<input type=\"number\" name=\"contendido\" value=\"".$row["contendido"]."\" class=\"form-control\"/>
           				<label>Archivo</label>
           				<input type=\"text\" name=\"archivo\" value=\"".$row["archivo"]."\" class=\"form-control\"/>
           				<div class='text-center'>        
            				<button type='submit' class='btn btn-info' name='op' id='op' value='modifica'>Modificar</button>
		    				<button type='reset' class='btn btn-default'>Cancelar</button>		
						</div>
       				</form>";
                    
                break;
				
				//modifica los datos de ponencias
				case 'modifica':
					echo "<h1> Modificar datos de ponencia </h1>";
					$pon->idPonencia = $id;
					$pon->idUser = $_REQUEST["idUser"];
					$pon->ponencia = $_REQUEST["ponencia"];
					$pon->eje = $_REQUEST["eje"];
					$pon->contendido = $_REQUEST["contendido"];
					$pon->archivo = $_REQUEST["archivo"];
					if ($pon->modifica())
					 {
						echo "<p class='text-primary'>Datos modificados correctamente</p>";
					}
					  else
					 {
						echo "<p class='text-danger'>Error en la modificacion de datos</p>";
					}
					
			   break;
               
			   case 'eliminar':
				   if ($pon->eliminar())
				   {
				   		echo "<p>Ponencia eliminada correctamente</p>";
				   }
				   else
				   {
					   echo "<p>Ponencia no eliminada</p>";
				   }
				break;
				
				case 'agregar':
					echo "<h2>Ponencia a agregar</h2>";
					include '../lib/usuarios.php';
					$user = new Usuarios;
					$row = $user->consulta($id);
					//datos del usuario
					echo "<h3 class='text-center'>Datos del usuario</h3>
							<p><b>Apellido:</b> ".$row["apellido"]
						 ." <b>Nombre:</b> ".$row["nombre"]
						 ." <b>Tipo DNI:</b> ".$row["tipoDoc"]." ".$row["doc"]
						 ." <b>Domicilio:</b> ".$row["direccion"]."</p>";
						 
					//formulario de carga
					echo "<form class='form-inline' role='form' action='ponencia.php' 
	 						enctype = 'multipart/form-data' method='post'>
	 						<label class='sr-only'>IdUser</label>
	 						<input type='hidden' name='id' value='".$id."' />
	 						<label class='sr-only'>Nombre de ponencia</label>
	 						<input type='text' class='form-control' name='ponencia' placeholder='Nombre de ponencia' required/>
	 						<label class='sr-only'> Contenidos</label>
	 						<input type='number' class='form-control' name='contenidos' placeholder='Contenidos' required/>
	 						<label class='sr-only'>Ejes</label>
	 						<input type='number' class='form-control' name='ejes' placeholder='Ejes' required/>
	 						<div class='form-group'>
								<input type='file' class='form-control' id='ponencia1F' name='ponencia1F' required/>
								<p class='help-block'><small>Formatos Permitidos: *.doc o *.docx</small></p>
							</div>						
							<div class='text-center'>
								<button type='submit' class='btn btn-info' name='op' id='op' value='confirma'>Agregar ponencia</button>
							</div> 
						</form>";
				break;
				
				case 'confirma':
					$pon->idUser = $_REQUEST["id"];
					$pon->ponencia = $_REQUEST["ponencia"]; 
					$pon->eje = $_REQUEST["ejes"];
					$pon->contendido = $_REQUEST["contenidos"];
					$pon->archivo = $_FILES['ponencia1F']["name"];
					//falta la carga de archivo
					$arch = $_FILES['ponencia1F']["name"];
					$dir = "../upload/arch".$pon->idUser."_".$arch;
					$tipos = array("application/msword",
            				"application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            				
            		if(in_array($_FILES['ponencia1F']['type'],$tipos))
            		{
            			if(copy($_FILES['ponencia1F']['tmp_name'], $dir))
            			{
            				echo "<p>Archivo subido correctamente</p>";
            				if($pon->nuevo())
							{
								echo "<p class='text-primary'>Ponencia cargada correctamente</p>";
							}
							else
				    		{
								echo "<p class='text-danger'>Ponencia no cargada correctamente, por favor 
										haga click <a href='ponencia.php?op=agregar&id=".$_REQUEST["idUser"]
										."'>aqui</a> para cargarlas nuevamente</p>";
							}
            			}
            			else
            			{
							echo "<p>Error en la subida de archivos</p>";	
						}
            		}
					else
					{
						echo "<p>Archivo en formato no permitido</p>";
					}		
					 
											
				break;
			    
                default:
                    
                    break;
            }
        ?>	
        
	   
	
    <div class="text-center">
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>
