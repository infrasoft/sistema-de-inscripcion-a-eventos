-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-02-2016 a las 20:35:35
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `incripcion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenidos`
--

CREATE TABLE IF NOT EXISTS `contenidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `esp` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `port` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=16 ;

--
-- Volcar la base de datos para la tabla `contenidos`
--

INSERT INTO `contenidos` (`id`, `esp`, `port`) VALUES
(1, 'Procesos de admisión  (individuos o grupos) ', ' Processos de Acolhimento (de indivíduos ou grupos);\r\n'),
(2, 'Los procesos de seguimiento (monitoreo terapéutico u otras formas de seguimiento) ', ' Processos de Acompanhamento (acompanhamento terapêutico e outras formas de acompanhamentos);\r\n'),
(3, 'Los procesos de evaluación (evaluación psicológica de los individuos, evaluación de los diagnósticos institucionales y sociales; Evaluación de la educación, evaluación del aprendizaje, evaluación de competencias) ', ' Processos de Avaliação (avaliação psicológica de indivíduos; avaliação para diagnósticos institucionais e sociais; avaliação de aprendizagem; avaliação de competências);\r\n'),
(4, 'Los procesos de comunicación (relacionados con los medios de comunicación, énfasis en los procesos de comunicación que involucran personas, grupos o instituciones) ', ' Processos de Comunicação (trabalhos relacionados aos meios de comunicação ou ênfase em processo de comunicação envolvendo indivíduos, grupos ou instituições);\r\n'),
(5, 'Procesos culturales (trabajo con las diversas formas de producción cultural,incluyendo las formas de expresión artística) ', ' Processos Culturais (diversas formas de produções culturais, incluídas as formas de expressão artística);\r\n'),
(6, 'Procesos educativos (educación / formación / orientación de los profesores, la planificación educativa, el desarrollo de proyectos educativos; evaluación de los procesos educativos, orientación vocacional, planificación y seguimiento de acciones educativa', ' Processos Educativos (formação/capacitação/orientação de professores; planejamento educacional; elaboração de projetos educacionais; avaliação de processos educativos; orientação profissional/vocacional; planejamento e acompanhamento de medidas socioeduc'),
(7, 'Procesos formativos (en diferentes áreas de formación, capacitación en diversos ámbitos) ', ' Processos Formativos (formação de profissionais de diferentes áreas; capacitação de trabalhadores de áreas diversas);\r\n'),
(8, 'Procesos formativos de Psicólogos (formación de psicólogos en el pregrado, postgrado, especialización) ', ' Processos Formativos de Psicólogos (formação de psicólogos na graduação, pós-graduação, especialização);\r\n'),
(9, 'Procesos Grupales (en diversas situaciones de dinámica de grupo, evaluación de los procesos de grupo) ', ' Processos Grupais (desenvolvimento de grupos em situações diversas; condução de dinâmicas de grupo; avaliação de processos grupais);\r\n'),
(10, 'Procesos de movilización social (actividades de participación social, desarrollo de las comunidades)', ' Processos de Mobilização Social (organização de grupos para atividades de participação social; desenvolvimento comunitário);\r\n'),
(11, 'Procesos de organización (de las organizaciones de trabajo, diversas formas de los procesos de organización institucionales o sociales) ', ' Processos Organizativos (atuação em organizações ou na área do trabalho, cuja ênfase seja as diversas formas de processos organizativos);\r\n'),
(12, 'Procesos de orientación y asesoramiento (individuos o grupos) ', ' Processos de Orientação e Aconselhamento (de indivíduos ou grupos);\r\n'),
(13, 'Planificación de Procesos y Gestión Pública (identificación y evaluación de las necesidades sociales, desarrollo y evaluación de planes de acción, desarrollo y evaluación de las políticas públicas) ', ' Processos de Planejamento e Gestão Pública (identificação e avaliação de demandas; elaboração e avaliação de planos de ação, desenvolvimento e avaliação de politicas públicas);\r\n'),
(14, 'Procesos terapéuticos (prácticas terapéuticas que implican individuos o grupos) ', ' Processos Terapêuticos (práticas terapêuticas envolvendo indivíduos ou grupos);\r\n'),
(15, 'Procesos investigativos( investigación destinadas a individuos o grupos) ', ' Processos Investigativos (pesquisa, trabalhos teóricos e empíricos)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejes`
--

CREATE TABLE IF NOT EXISTS `ejes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `esp` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `port` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=22 ;

--
-- Volcar la base de datos para la tabla `ejes`
--

INSERT INTO `ejes` (`id`, `esp`, `port`) VALUES
(1, 'El sujeto latinoamericano, producción teórica y profesional para la psicología. ', ' O tema da América Latina, a produção teórica e profissional para a psicologia.\r\nO sujeito latino-americano, a produção teórica e profissional para a psicologia.'),
(2, 'Los debates de la psicología en América Latina, la subjetividad de la época y el campo de la singularidad. ', 'As discussões da psicologia na América Latina, a subjetividade do tempo e domínio da singularidade.\r\nOs debates da psicologia na América Latina, a subjetividade do tempo e o campo da singularidade.'),
(3, 'La construcción del conocimiento en psicología, en América Latina. ', '  A construção do conhecimento em psicologia na América Latina.\r\nA construção do conhecimento em psicologia na América Latina.'),
(4, 'Memoria histórica, construcción de subjetividad y reparación social. ', ' Remembrance, construção da subjetividade e reparação social.\r\nMemória da Psicologia, construção da subjetividade e reparação social.'),
(5, 'La psicología y el enfoque diferencial. ', ' Remembrance, construção da subjetividade e reparação social.\r\nA Psicologia e o enfoque da abordagem diferencial.'),
(6, 'Psicología y violencias en América Latina. Investigación, intervención y acompañamiento. ', ' Psicologia e violência na América Latina. Pesquisa, intervenção e apoio.\r\nPsicologia e violência na América Latina. Pesquisa, intervenção e apoio.'),
(7, 'Estudios de Género y Subjetividad. ', ' Estudos de Gênero e Subjetividade.\r\nEstudos de Gênero e Subjetividade.'),
(8, 'Diferencias y desigualdades de los pueblos de América Latina. El rol profesional del psicólogo y psicóloga. ', ' As diferenças e as desigualdades dos povos da América Latina. O papel profissional do psicólogo e psicólogo.\r\nAs diferenças e as desigualdades dos povos da América Latina. O papel profissional do psicólogo e psicólogo.'),
(9, 'Las migraciones, sus efectos y la contribución de la psicología. ', ' Migração, seus efeitos e a contribuição da psicologia.\r\nMigração, seus efeitos e a contribuição da psicologia.'),
(10, 'Psicología latinoamericana y movimientos sociales. ', ' Psicologia e movimentos sociais latino-americanos.\r\nPsicologia latino-americana e movimentos sociais.'),
(11, 'Pueblos originarios e interculturalidad. Aportes y abordajes desde la psicología. ', 'Povos indígenas e multiculturalismo. Contribuições e abordagens da psicologia.'),
(12, 'Psicología y políticas públicas. ', ' Psicologia e de política pública.\r\n Psicologia e políticas públicas.'),
(13, 'La realidades complejas de América Latina y las prácticas de la psicología. ', 'As complexas realidades da América Latina e as práticas da psicologia.'),
(14, 'Psicologia, cultura y lenguajes artísticos', 'Psicologia, cultura e linguagens artísticas'),
(15, 'Problemáticas de la formación en psicología. ', 'As questões da formação em psicologia.'),
(16, 'La evaluación psicológica. Su desarrollo y aplicación en América Latina.', 'A avaliação psicológica. Desenvolvimento e aplicação na América Latina.'),
(17, 'El ejercicio Profesional en los contextos locales. Desafíos en la regulación del ejercicio  profesional. ', 'O exercício profissional nos contextos locais. Desafios na regulamentação da profissão.'),
(18, 'Ética y deontología profesional del psicólogo latinoamericano. ', 'Ética e compromisso profissional do psicólogo latino-americano.'),
(19, 'La intervención profesional en los diferentes campos de la psicología. ', ' A intervenção profissional em diferentes campos da psicologia.\r\nA intervenção profissional nos diferentes campos da psicologia.'),
(20, 'Psicología y las problemáticas de la niñez, la adolescencia y adultos mayores en América Latina. ', ' Psicologia e os problemas das crianças, adolescentes e adultos mais velhos na América Latina.\r\nPsicologia e as questões das crianças, adolescentes e idosos na América Latina.'),
(21, 'Psicología e investigación. Desafíos. ', ' Psicologia e pesquisa. Desafios.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE IF NOT EXISTS `pago` (
  `idPago` int(11) NOT NULL AUTO_INCREMENT,
  `forma` enum('tarjeta','deposito','efectivo') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `dato` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'cantidad de cuotas si es tarjeta - nro de transaccio si es deposito bancario',
  `importe` float DEFAULT NULL,
  PRIMARY KEY (`idPago`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `pago`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ponencia`
--

CREATE TABLE IF NOT EXISTS `ponencia` (
  `idPonencia` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) DEFAULT NULL,
  `ponencia` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `eje` int(11) DEFAULT NULL,
  `contendido` int(11) DEFAULT NULL,
  `archivo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`idPonencia`),
  KEY `fk_idUser` (`idUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `ponencia`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socio`
--

CREATE TABLE IF NOT EXISTS `socio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entidad` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=61 ;

--
-- Volcar la base de datos para la tabla `socio`
--

INSERT INTO `socio` (`id`, `entidad`) VALUES
(0, 'No Asociado'),
(1, 'Asociación Argentina de Estudio e Investigación en Psicodiagnóstico (ADEIP)\r\n'),
(2, 'Asociación Argentina de Psicodiagnostico de Rorschach (AAPRO)\r\n'),
(3, 'Asociación de Psicólogos de Buenos Aires (APBA)\r\n'),
(4, 'Asociación de Psicologos Laborales de Argentina (APSILA)\r\n'),
(5, 'Federación de Psicólogos de la República Argentina (FePRA)\r\n'),
(6, 'Asociación Argentina de Psicología Jurídica y Forense (AAPJYF)\r\n'),
(7, 'Colegio Nacional de Psicólogos de Bolivia\r\n'),
(8, 'Associação Brasileira de Editores Científicos em Psicologia (ABECIPSI)\r\n'),
(9, 'Associação Brasileira de Ensino de Psicologia (ABEP) \r\n'),
(10, 'Associação Brasileira de Orientação Profissional (ABOP)\r\n'),
(11, 'Associação Brasileira de Psicologia do Desenvolvimento  (ABPD) \r\n'),
(12, 'Associação Brasileira de Psicologia Escolar e Educacional (ABRAPEE) \r\n'),
(13, 'Associação Brasileira de Psicologia Organizacional e do Trabalho (SBPOT) \r\n'),
(14, 'Associação Brasileira de Psicologia Política (ABPP)\r\n'),
(15, 'Associação Brasileira de Psicologia Social  (ABRAPSO)\r\n'),
(16, 'Associaçao Brasileira de Roscharch e Métodos Proyectivos (ASBRo)\r\n'),
(17, 'Associação Nacional de Pesquisa e Pós Graduação em Psicologia (ANPEPP)\r\n'),
(18, 'Conselho Federal de Psicologia (CFP)\r\n'),
(19, 'Conselho Regional de Psicologia 01ª Região – Distrito Federal do Brasil\r\n'),
(20, 'Conselho Regional de Psicologia 02ª Região – Pernambuco / Fernando de Noronha\r\n'),
(21, 'Conselho Regional de Psicologia 04ª Região – Minas Gerais \r\n'),
(22, 'Conselho Regional de Psicologia 05ª Região – Rio de Janeiro\r\n'),
(23, 'Conselho Regional de Psicologia 06ª Região – São Paulo\r\n'),
(24, 'Conselho Regional de Psicologia 07ª Região – Rio Grande do Sul\r\n'),
(25, 'Conselho Regional de Psicologia 12ª Região – Santa Catarina\r\n'),
(26, 'Conselho Regional de Psicologia 14ª Região – Mato Grosso\r\n'),
(27, 'Conselho Regional de Psicologia 15ª Região – Alagoas\r\n'),
(28, 'Federação Latino-Americana de Análise Bioenergética (FLAAB)\r\n'),
(29, 'Federação Nacional dos Psicólogos  (FENAPSI) \r\n'),
(30, 'Instituto Brasileiro de Avaliação Psicológica (IBAP) \r\n'),
(31, 'Instituto Silvia Lane (ISL) \r\n'),
(32, 'Sindicato dos Psicólogos no Estado da Bahia (SINPSIBA)\r\n'),
(33, 'Sindicato dos Psicólogos no Estado de São Paulo (SinPsi)\r\n'),
(34, 'Sociedade Brasileira de Psicologia e Acupuntura (SOBRAPA) \r\n'),
(35, 'Sociedade Brasileira de Psicologia Hospitalar (SBPH) \r\n'),
(36, 'Centro de Pensamiento y Acción Crítica de Valparaíso (CEPAC)\r\n'),
(37, 'Cátedra Libre Martín Baró \r\n'),
(38, 'Colegio de Psicólogos de Costa Rica\r\n'),
(39, 'Sociedad Cubana de Psicología\r\n'),
(40, 'Sociedad Cubana de Psicología de la Salud\r\n'),
(41, 'PSICOLEGAS de El Salvador\r\n'),
(42, 'Colectivo de Investigaciones Sociales y Laborales (COISOLA)\r\n'),
(43, 'Asociación Guatemalteca de Psicología (AGP)\r\n'),
(44, 'Colegio de Psicólogos de Guatemala\r\n'),
(45, 'Psico-Acción\r\n'),
(46, 'Asociación de Alternativas en Psicología (AMAPSI)\r\n'),
(47, 'Asociación de Egresados de Psicología Social (AEPSO)\r\n'),
(48, 'Colegio de Profesionales de la Psicología del Estado de Jalisco, AC\r\n'),
(49, 'Colegio de Psicólogos de Tlaxcala en Movimiento\r\n'),
(50, 'Federación Nacional de Colegios, Sociedades y Asociaciones de Psicólogos de México AC (FENAPSIME)\r\n'),
(51, 'Instituto Internacional de Investigación, Evaluación y Rehabilitación en Retroalimentación Biológica y Neuroretroalimentación Aplicada\r\n'),
(52, 'Sociedad Paraguaya de Psicología\r\n'),
(53, 'Colegio de Psicologos del Peru\r\n'),
(54, 'Colectivo Boricua de Psicología de la Liberación\r\n'),
(55, 'Asociación de Psicología del Trabajo del Uruguay (ADEPTRU)\r\n'),
(56, 'Cordinadora de Psicólogos de Uruguay (CPU)\r\n'),
(57, 'Sociedad de Psicología del Uruguay\r\n'),
(58, 'Sociedad Uruguaya de Análisis y Modificación de la Conducta (SUAMOC)\r\n'),
(59, 'Colectivo Psicologos y Psicologas por el Socialsmo \r\n'),
(60, 'Asociación Latinoamericana para la formación y la enseñanza de la Psicología (ALFEPSI)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `pass` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `verif` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcar la base de datos para la tabla `user`
--

INSERT INTO `user` (`usuario`, `pass`, `verif`) VALUES
('mario', 'b32d8f835715fd797356784a14efe7bda0d4b39c', 'wCEw58I2Khv0wwq8tGssmd9mmJilfLuvbbGtPbolCBvD0SwxxS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(80) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `tipoDoc` enum('Otro','Pasaporte','LE','LC','CI','RG','CIC','VISA','CC','CIE','DNI') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `doc` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `pais` varchar(130) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `provincia` int(11) DEFAULT NULL,
  `cp` varchar(80) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(120) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `categoria` enum('expositor','estudiante','conferencista','participante','coautor','autor') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `leng` enum('port','esp') COLLATE utf8_spanish2_ci DEFAULT 'esp' COMMENT '1: si es español, 2 si es portugues',
  `socio` int(11) DEFAULT '0' COMMENT '0 si no es socio',
  `verificado` enum('no','si') COLLATE utf8_spanish2_ci DEFAULT 'no' COMMENT 'correo y datos verificado',
  `presente` enum('si','no') COLLATE utf8_spanish2_ci DEFAULT 'no' COMMENT 'si presento al evento estado',
  `emitido` enum('no','si') COLLATE utf8_spanish2_ci DEFAULT 'no' COMMENT 'se emitio certificado ',
  `codigo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'codigo de seguridad de verificacion',
  `idPago` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `idPago` (`idPago`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `usuarios`
--


--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `ponencia`
--
ALTER TABLE `ponencia`
  ADD CONSTRAINT `fk_idUser` FOREIGN KEY (`idUser`) REFERENCES `usuarios` (`idUsuario`);
