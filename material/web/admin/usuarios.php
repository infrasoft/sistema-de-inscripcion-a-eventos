<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 
 //librerias requeridas
 include '../lib/sql.5.5.php';
 include '../lib/usuarios.php';
 
 //inicializar variables
 if (!isset($_REQUEST["id"]))
 {
     $idUsuario = 0;
 }
  else
 {
     $id = $_REQUEST["id"];
	 $user= new Usuarios;
	 $consulta = $user->consulta($id);
 }
 
 if (isset($_REQUEST['leng'])) //aqui se seleccionara el idioma del participante
 {//portugues
 	$leng = "port";
     $data = array( "nombre"=>"Nome  (*)",
 				"apellido" => "Sobrenome (*) ",
                "tipoDoc" =>"Tipo de documento: ",
                "nroDoc" => "Número do Documento (*) ",
                "pais" => "Pais ",
                "provincia" => "Provincia",
                "ciudad" => "Cidade (*) ",
                "direccion" => "Direccion (*) ",
                "cp" => "Código postal (*) ",
                "telefono" => "Telefone (*) ",
                "correo" => "e-mail (*) ",
                "socio" => "Associado ",
                "formasP" => "Método de pago ",
                	"efectivo" => "Espécie ",
                	"deposito"=> "Depósito bancário",
                	"tarjeta" => "Cartão Credito  ",
                "transaccion" => "Nr. Transação ",
                "tarjetaP" => "Cantidad de Cuotas",
                "importe" => "Valor (*)",
                "incripcionG" => "inscripción Grupal ",
                "cant" => "Número de pessoas para se registrar",
                "categoria" => "Categoria ",
                	"autor"=> "Autor",
                	"coautor" => "Co-Autor",
                	"participante" => "Participante",
                	"conferencista" => "Conferencista",
                	"estudiante" => "Estudante",
                	"expositor" => "Expositor Convitado",
                "entidad" => "Entidad ",
                "ponencia1" => "Trabalho 1 ",
                "ponencia2" => "Trabalho 2 ",
                "ponencia3" => "Trabalho 3 ",
                "mensaje" =>"Por favor preencha o seguinte formulário para inscrever-se no: VI Congreso ULAPSI.
								Se tiver qualquer duvida ou comentário por favor escreva ao  congreso@ulapsi.org",
                "datosObli" => "Algunos campos filho obligatorios (*):",
                "idioma"=>"Cambio de idioma",
				"titulo" => "Ficha de inscrição");
 } 
 else
 {		//español
 	 $leng = "esp";
     $data = array( "nombre"=>"Nombre: (*) ",
 				"apellido" => "Apellido (*) ",
                "tipoDoc" =>"Tipo de documento: ",
                "nroDoc" => "Numero de Documento (*) ",
                "pais" => "Pais ",
                "provincia" => "Provincia ",
                "ciudad" => "Ciudad (*) ",
                "direccion" => "Direccion (*) ",
                "cp" => "Codigo Postal (*) ",
                "telefono" => "Telefono (*) ",
                "correo" => "Correo Electonico (*) ",
                "socio" => "Socio ",
                "formasP" => "Formas de Pago ",
                	"efectivo" => "Efectivo",
                	"deposito"=> "Deposito Bancario",
                	"tarjeta" => "Tarjetas de credito",
                "transaccion" => "Nro de Transaccion ",
                "tarjetaP" => "Cantidad de Cuotas ",
                "importe" => "Importe (*) ",
                "incripcionG" => "Inscripcion Grupal ",
                "cant" => "Cantidad de personas a inscribir",
                "categoria" => "Categoria ",
                	"autor"=> "Autor",
                	"coautor" => "Co-Autor",
                	"participante" => "Participante",
                	"conferencista" => "Conferencista",
                	"estudiante" => "Estudiante",
                	"expositor" => "Expositor Invitado",
                "entidad" => "Entidad ",
                "ponencia1" => "Ponencia 1  ",
                "ponencia2" => "Ponencia 2 ",
                "ponencia3" => "Ponencia 3 ",
                "mensaje" =>"Por favor complete el siguiente formulario de inscripcion para el VI Congreso ULAPSI.
                			 Dudas o consultas puede hacerlo al siguiente mail congreso@ulapsi.org",
                "datosObli" => "Algunos campos son obligatorios (*):",
                "idioma"=>"Cambio de idioma",
				"titulo" => "Formulario de Inscripción");
 }
 
 ?>
  <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
    	<div class="text-center">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        </div>
        <hr />
    	<h1>Datos de inscripcion</h1>
    <form class="form-inline" role="form" action="control.php" 
	  method="post">
	  	<div class="form-group">    		
    		
    		<div class="form-group">
    		<label><?php echo $data["apellido"]?></label>
    		<input type="text" value="<?php echo $consulta["apellido"];?>" class="form-control" name="apellido"/>
    		
    		<label><?php echo $data["nombre"]?></label>
    		<input type="text" value="<?php echo $consulta["nombre"];?>" class="form-control" name="nombre"/>
    		
    		<label><?php echo $data["tipoDoc"]?></label>
    		<select class="form-control" name="tipoDoc">
	            <option>DNI</option>
            	<option>CC</option>
            	<option>CI</option>
            	<option>CIC</option>
            	<option>CIE</option>
            	<option>LC</option>
	            <option>LE</option>
            	<option>Pasaporte</option>
	            <option>RG</option>
            	<option>VISA</option>
            	<option>Otro</option>
       		</select>
       		</div>
       		
       		<div class="form-group">
       			<label><?php echo $data["nroDoc"]?></label>
       			<input type="text" value="<?php echo $consulta["doc"];?>" class="form-control" name="doc"/>
       			
       			<label><?php echo $data["pais"]?></label>
       			<input type="text" value="<?php echo $consulta["pais"];?>" class="form-control" name="pais"/>
       			
       			<label> <?php echo $data["provincia"]?></label>
       			<select class="form-control" name="provincia">
                	<option value="1" selected="selected">Buenos Aires</option>
                	<option value="2">C.A.B.A.</option>
                	<option value="3">Catamarca</option>
                	<option value="4">Chaco</option>
                	<option value="5">Chubut</option>
                	<option value="6">Cordoba</option>
                	<option value="7">Corrientes</option>
                	<option value="8">Entre Rios</option>
                	<option value="9">Formosa</option>
                	<option value="10">Jujuy</option>
                	<option value="11">La Pampa</option>
                	<option value="12">La Rioja</option>
                	<option value="13">Mendoza</option>
                	<option value="14">Misiones</option>
                	<option value="15">Neuquen</option>
                	<option value="16">Rio Negro</option>
                	<option value="17">Salta</option>
                	<option value="18">San Juan</option>
                	<option value="19">San Luis</option>
                	<option value="20">Santa Cruz</option>
                	<option value="21">Santa Fe 1ra. Circ.</option>
                	<option value="23">Santiago del Estero</option>
                	<option value="24">Tierra del Fuego</option>
	                <option value="25">Tucuman</option>
                	<option value="22">Santa Fe 2da. Circ</option>
                	<option value="0">No Especificado</option>
               	</select>
       		</div>	
       		
       		<div class="form-group">
       			<label><?php echo $data["cp"]?></label>
       			<input type="text" class="form-control" name="cp" value="<?php echo $consulta["cp"]?>" required/>
       			
       			<label><?php echo $data["direccion"]?></label>
       			<input type="text" class="form-control" name="direccion" value="<?php echo $consulta["direccion"]?>" required/>
       			
       			<label><?php echo $data["telefono"]?></label>
       			<input type="text" class="form-control" name="telefono" value="<?php echo $consulta["telefono"]?>" required/>
       		</div>	
       		
       		<div class="form-group">
       			<label><?php echo $data["correo"]?></label>
       			<input type="email" class="form-control" name="email" value="<?php echo $consulta["email"]?>" required/>
       			
       			<label><?php echo $data["categoria"]?></label>
       			<select class="form-control" name="categoria">
                <option value="autor"><?php echo $data["autor"]?></option>
                <option value="coautor"><?php echo $data["coautor"]?></option>
                <option value="participante"><?php echo $data["participante"]?></option>
                <option value="conferencista"><?php echo $data["conferencista"]?></option>
                <option value="estudiante"><?php echo $data["estudiante"]?></option>
                <option value="expositor"><?php echo $data["expositor"]?></option>
            	</select>
       			       			
       		</div>
       		<hr />
       		<div class="text-center">
       			<button type="submit" class="btn btn-info" name="enviar" id="enviar">Confirmar</button>
		    	<button type="reset" class="btn btn-default">Cancelar</button>	
       		</div>
    	</div>
    </form>
    </div>
</body>
</html>