<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 
 //librerias requeridas
 include '../lib/sql.5.5.php';
 include '../lib/ponencia.php';
 
  //inicializar variables
 if (!isset($_REQUEST["id"]))
 {
     $idUsuario = 0;
 }
  else
 {
     $id = $_REQUEST["id"];
	 $pon= new Ponencia;
	 $pon->campos = "idPonencia,".$pon->campos;
	 $consulta = $pon->consulSQLbasica();
 }
 
 ?>
 <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <hr />
        
        

<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Inscriptos</a></li>
      <li><a href="#">Ponencias</a></li>
      <li><a href="#">Pagos</a></li>
      <li><a href="#">Ejes</a></li>
      <li><a href="#">Contenidos</a></li>
     
    </ul>
 	<div class="table-responsive">
    <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
        <select class="form-control" name="campo">
        	<option></option>
        	<option></option>
        	<option></option>
        	<option></option>
        </select>
      </div>
      <button type="submit" class="btn btn-default">Buscar</button>
    </form>
 	
    
  </div>
</nav>
	<div class="text-center">
		<h1> Lista de Ponencias</h1>
	</div>
	<table class="table table-striped">
		<tr>
			<td><b>Id Ponencia</b></td>
			<td><b>Id Usuario</b></td>
			<td><b>Titulo</b></td>
			<td><b>Eje</b></td>
			<td><b>Contenido</b></td>
			<td><b>Archivo</b></td>			
			<td><b>Opciones</b></td>
		</tr>
		<?php
		while($row = $consulta->fetch_assoc())
		{
 		   echo "<tr>
 		   			<td>".$row['idPonencia'] .
 		   		   '</td>
 		   		   <td>'.$row['idUser'].
				   "</td>
				   <td>".$row['ponencia'].
				   "</td>
				   <td>".$row['eje'].
				   "</td>
				   <td>".$row['contendido'].
				   "</td>
				   <td>".$row['archivo'].
				   			   		
				   "</td>
				   <td>
				   	<a href='../upload/arch".$row['idUser']."_".$row['archivo']."'>
				   		<span class='glyphicon glyphicon-download'> </span>
				   	</a> <!-- Descargar -->
				   	<a href='../upload/arch".$row['idUser']."_".$row['archivo']."'>
				   		<span class='glyphicon glyphicon-pushpin'> </span>
				   	</a> <!-- Modificar -->
				   </td>
				 </tr>";
		} 
		?>
	</table>
        
    </div>
    <div class="text-center">
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>
