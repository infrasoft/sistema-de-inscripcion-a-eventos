<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 
 //librerias requeridas
//include '/lib/sql.5.5.php'; 
//include '/lib/usuarios.php';
//include '/lib/pago.php';
//include '/lib/ponencia.php'; 
include '/lib/seguridad.php';

//clase para acceder a la base de datos
class SQL {
    //instancias
    public $tabla = "";
    public $campos = "";
    public $valores = "";
    public $condicion = "";
    public $opcion = 0;
    public $estilo = "";
    public $id = "";
    public $muestra = "";
    public $predeterminado = "";
    public $add = "";
    private $dbhost = "localhost";
    private $dbusername = "fepraadm_ariel";
    private $dbpass = "Eracles1092";
    private $dbname = "fepraadm_inscripcion";

    // conecta a la base de datos
    function __construct()
    {
        mysql_set_charset('utf8'); 
    }
    
    //conecta la base de datos
    public function conecta()
    {
        $mysqli = new mysqli($this->dbhost, $this->dbusername, $this->dbpass, $this->dbname); 
        if($mysqli->connect_errno > 0)
        {    
              die("Imposible conectarse con la base de datos [" . $mysqli->connect_error . "]");   
        }
        return $mysqli;
    }
    //Realiza una consulta basica
    //$conexion, $tabla, $campos, $condicion
    function consulSQLbasica() 
    {
        if ($this -> condicion == "") 
        {
            $query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . ";";
        }
         else
        {
            $query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
        }   
        $t = (strlen($query)-6)* -1;/* Esto es por poblemas de codificacion*/
        $query= substr(utf8_decode($query),$t);
        
        //echo  $query."<br/>";  //echo $query." ".$t;
        
        $mysqli = $this->conecta();     
        $consulta = $mysqli->query($query); 
        if($mysqli->errno) die($mysqli->error);             
        return $consulta;
    }

    // Realiza una consulta basica y devuelve el array
    function consultaSQLbasicaRow() 
    {
        $consulta = $this -> consulSQLbasica();
        $row = $consulta->fetch_assoc(); 
        return $row;
    }

    // Realiza la insercion de datos
    //$conexion,$tabla, $campos, $valores
    function insertaSQL() 
    {
        $query = "INSERT INTO ".$this -> tabla." (".$this -> campos .") VALUES (".$this -> valores.");";
        //echo  $query." <br />";
        $mysqli = $this->conecta();
        $consulta = $mysqli->query($query);
        if($mysqli->errno) die($mysqli->error); 
        return $consulta;
    }

    // Realiza la modificacion de datos
    //$conexion, $tabla, $campos, $valores, $condicion
    function modificarSQL() 
    {
        $query = "UPDATE " . $this -> tabla . " SET ";
        $campos = utf8_decode($this -> campos);
        $atributos = explode(",", $campos);
        $val = explode(",", $this -> valores);
        $j = 0;
        foreach ($atributos as $i) 
        {
            $query .= "$i = $val[$j],";
            $j++;
        }
        
        $query = substr($query, 0, strlen($query) - 1);
        
        if ($this -> condicion != "") {
            $query .= " WHERE ";
            $query .= $this -> condicion;
        }
        $query .= ";";
        //echo $query;
        $mysqli = $this->conecta();
        $consulta = $mysqli->query($query);
        if($mysqli->errno) die($mysqli->error); 
        return $consulta;
    }

    //Realiza el borrado de datos de una base de datos
    //$conexion, $tabla, $condicion
    function BorraSQL() 
    {
        $query = "DELETE FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
        //echo $query;
        $mysqli = $this->conecta();
        $consulta = $mysqli->query($query);
        if($mysqli->errno) die($mysqli->error);         
        return $consulta;
    }

    // Muestra un select de una tabla predeterminada, devolviendo el valor
    //$conexion, $tabla, $id, $muestra, $condicion, $predeterminado, $estilo
    function selectTabla()
     {
        $devuelve = "<select name='" .$this -> tabla.$this -> add."' class='" . $this -> estilo . "'>\n";
        $consulta = $this ->consulSQLbasica();
        $aux = explode(",", $this -> muestra);

        $row = $consulta->fetch_assoc();
        while ($row != null) {
            if ($row[$this->id] == $this -> predeterminado) 
            {
                $devuelve .= "<option value='" . $row[$this -> id] . "' selected='selected'>";
                foreach ($aux as $i) 
                {
                    $devuelve .= utf8_encode($row[$this->muestra]);
                }
                $devuelve .= "</option>\n";
            } 
            else 
            {
                $devuelve .= "<option value='" . $row[$this -> id] . "'>";
                foreach ($aux as $i)
                {
                    $devuelve .= utf8_encode($row[$this -> muestra]);
                }
                $devuelve .= "</option>\n";
            }
            $row = $consulta->fetch_assoc();
        }
        $devuelve .= "</select>\n";
        if ($this -> opcion == 0) 
        {
            return $devuelve;
        } 
        else 
        {       
            echo $devuelve;
        }
    }
}

/**
  *  Clase para el manejo de usuarios
  */
 class Usuarios extends SQL
 {
 	public $apellido = "";
	public $nombre = "";
	public $tipoDoc = 0; 
	public $nroDoc = ""; 
	public $pais = ""; 
	public $provincia = 0; 
	public $ciudad = "";     
	public $cp = ""; 
	public $direccion = ""; 
	public $telefono = ""; 
	public $correo = "";
	public $categoria = 0;
    public $leng = 1; //español por defaul
    public $socio = 0; //no es socio por defaul
    public $verificado = 0;
    public $presente = 0; 
    public $emitido  = 0;   
    public $codigo = "";
    public $idPago = 0;
     //constructores
     function __construct()
	 {
	    $this->tabla = "usuarios";
     	$this->campos = "apellido,nombre,tipoDoc,doc,pais,provincia,cp,direccion"
     	                .",telefono,email,categoria,leng,socio,verificado,"
     	                ."presente,emitido,codigo,idPago";    
     }
	 
	 //nuevo usuario
	 public function Nuevo()
	 {
		 $this->valores = "'".$this->apellido."','".$this->nombre."','".$this->tipoDoc
                          ."','".$this->nroDoc."','".$this->pais."',".$this->provincia  
                          .",'".$this->cp."','".$this->direccion."','".$this->telefono
                          ."','".$this->correo."','".$this->categoria."','".$this->leng
                          ."',".$this->socio.",".$this->verificado.",".$this->presente
                          .",".$this->emitido.",'".$this->codigo."',".$this->idPago;
		 return $this->insertaSQL();
	 }
     
     //consulta los datos de un usuario
     public function consulta($id)
     {
         $this->condicion = "idUsuario=".$id;
         return $this->consultaSQLbasicaRow();
     }
     
     //modifica datos del usuario
     public function modifica()
     {
         $this->valores = "'".$this->apellido."','".$this->nombre."','".$this->tipoDoc
                          ."','".$this->nroDoc."','".$this->pais."',".$this->provincia  
                          .",'".$this->cp."','".$this->direccion."','".$this->telefono
                          ."','".$this->correo."','".$this->categoria."','".$this->leng
                          ."',".$this->socio.",".$this->verificado.",".$this->presente
                          .",".$this->emitido.",'".$this->codigo."',".$this->idPago;
         return $this->modificarSQL();
     }
     // elimina un usuario. Ver el efecto cadena
 }
 
 /**
  *  Clase de pagos del usuario
  */
 class Pagos extends SQL
 {
     public $idPago = 0;    
     public $forma = "";
     public $dato = "";
     public $importe = 0;
     
     function __construct() 
     {
        $this->tabla = "pago";
        $this->campos = "forma,dato,importe";    
     }
     
     //ingresa un nuevo metodo de pago
     public function nuevo()
     {
         $this->valores = "'".$this->forma."','".$this->dato."',".$this->importe;
         return $this->insertaSQL();
     }
     
     //realiza una consulta de un pago realizado
     public function consulta($idPago)
     {
         $this->condicion = "idPago=".$idPago;
         return $this->consultaSQLbasicaRow();
     }
 }
 
 /**
  *  Clase para las ponencias de los usuarios
  */
 class Ponencia extends SQL 
 {
     public $idPonencia = 0;
     public $idUser = 0;
     public $ponencia = "";
     public $eje = 0;
     public $archivo = "";
     function __construct()
	 {
         $this->tabla = "ponencia";
		 $this->campos = "idUser,ponencia,eje,archivo";
     }
	 
	 //registra una nueva ponencia
	 public function nuevo()
	 {
		 $this->valores = $this->idUser.",'".$this->ponencia."',".
		                  $this->eje.",'".$this->archivo."'";
		 return $this->insertaSQL();
	 }
 }

 // clase creada para la seguridad del sistema
class Seguridad extends SQL{
	//instacias
	public $user="";
	public $pass="";
	public $verif="";
	public $verifica="";
	public $logueado=FALSE;
	public $intentos =0;	
	public $tiempo=2000;
	public $hora = "";
	private $permitidos = ""; 
	
	public function  __construct()
	{
		$this->tabla = "user";
        $this->campos = "usuario,pass,verif";        	
	}
	
	//verifica si el login es correcto
	public function login($user,$pass)
	{
        $pass = $this->encriptar($pass);
		$this->condicion ="usuario='$user' AND pass='$pass'";	
        $row = $this->consultaSQLbasicaRow();
        //echo $pass." ".$this->encriptar($pass);
        if(is_array($row))
        {            
            $this->user = $row["usuario"];
			$this->pass = $row["pass"];
			$this->verif = $this->encriptar($row["verif"]);
			$this->verifica = $this->encriptar($row["usuario"]).
							  $this->encriptar($row["pass"]).
							  $row["verif"];
			$this->logueado = TRUE;
			$this->hora=time();
			//echo $this->verifica;
        }
        else
        {
            $this->intentos = $this->intentos + 1;
        }
	}
	
	//verifica la actividad del usuario, si esta o no frente al sistema
	public function verifica()
	{		
		if (($this->logueado == 1) Or
			(($this->encriptar($this->user) + 
			$this->encriptar($this->pass) +
			$this->encriptar($this->verif))== $this->verifica)) 
		{
			$this->hora = time();
			
		}
		 else
	    {
	    	if (($this->hora + $this->tiempo)>= time())
			{
				$this->hora = time();
			}
			 else
			 {
				$this->user = "";
				$this->pass = "";
				$this->verif = "";
				$this->verifica = "";
				$this->logueado = FALSE;
			}
			header('Location: login.php');			
		}
		//$this->Imprime();
	}
	
	// muestra todos los datos del objeto
	public function Imprime()
	{
		echo "Usuario: ".$this->user."<br/>".
			 "Password: ".$this->pass."<br/>".
			 "Verif: ".$this->verif."<br/>".
			 "Verificacion: ".$this->verifica."<br/>".
			 "Logueado: ".$this->logueado."<br/>";
	}		 
	//verifica que los datos ingresados no sea maliciosos
	public function texto($var)
	{
		return filter_var($var,FILTER_SANITIZE_EMAIL);
	}	
	
	// verifica que el ingreso solo sean numeros
	public function numeros()
	{
		
	}
	
	// genera un vector de numeros aleatorios, sin repetir 
    function generaAleatios($cantidad, $tope)
    {
        $aux[] = null;
        $num = rand(1,$tope);
        for ($i=0; $i < $cantidad; $i++)
        {
            if(in_array($num, $aux))
            {
                $i--;
            }
            else
            {
               $aux[$i] =  $num;
            }
            $num = rand(1,$tope);
        }               
        return $aux;        
    }
    
     //Genera una cadena aleatoria para confirmar seccion
    public function generaCadena($num)
    {
        $cad = '';
        $codigo = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($i=0; $i< $num;$i++)
        {
			$pos = rand(1,55);            
            $cad .=  $codigo[$pos];
        }        
        return $cad;
    }
	
	//encripta los numeros de una manera unica
	function encriptar($var)
	{
		$n = strlen($var);
		$codigo = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';		
		$encrip = sha1(crypt($var,$codigo));
		return $encrip;
	}
}
 
session_start();
//inicializar variables
$pagoMje = "";
$ponenciaMje = "";
$mje = "";
$mail = "";
if (!isset($_SESSION["importe"])) 
 {
     $_SESSION["importe"] = ""; // si        
     $_SESSION["efectivo"] = "";// si
     $_SESSION["deposito"] = "";// si
     $_SESSION["tarjeta"] = "";// si
     $_SESSION["idUsuario"] = "";
     $_SESSION["personas"] = "";// si
     $_SESSION["pago"] = 0; // idPago
 }

	//echo "datos:".$_SESSION["importe"]." ".$_SESSION["efectivo"]." ".$_SESSION["deposito"]." ".$_SESSION["tarjeta"]." ".$_SESSION["idUsuario"]." ".$_SESSION["personas"]." idpago:".$_SESSION["pago"];

//obtener datos
if(isset($_POST['enviar']))
{
        $mail = "Datos del usuario: \n";
    $user = new Usuarios;
    //Guardamos en variables los datos enviados
    $user->apellido = $_POST['apellido'];
        $mail .= "Apellido: ".$user->apellido."\n";
    $user->nombre = $_POST['nombre'];
        $mail .= "Nombre: ".$user->nombre."\n";
    $user->tipoDoc = $_POST['tipoDoc'];
        $mail .= "Tipo Doc: ".$user->tipoDoc."\n";
    $user->nroDoc = $_POST['nroDoc'];
        $mail .= "Nro Doc: ".$user->nroDoc."\n";
    $user->pais = $_POST['pais'];
        $mail .= "Pais:".$user->pais."\n";
	if ($user->pais == "Argentina")
	{
		$user->provincia = $_POST['provincia'];
	}
	 else
    {
		$user->provincia = 0;
	}    
        $mail .= "Provincia : ".$user->provincia."\n";
    $user->ciudad = $_POST['ciudad'];
        $mail .= "Ciudad : ".$user->ciudad."\n";
    $user->direccion = $_POST['direccion'];
        $mail .= "Direccion : ".$user->direccion."\n";
	$user->cp = $_POST['cp'];
        $mail .= "Codigo Postal: ".$user->cp."\n";
	$user->telefono = $_POST['telefono'];
        $mail .= "Telefono: ".$user->telefono."\n";
	$user->correo = $_POST['correo'];
        $mail .= "Correo: ".$user->correo."\n";
	$user->categoria = $_POST['categoria'];
        $mail .= "Categoria: ".$user->categoria."\n";
	$user->leng = $_POST['leng'];
        $mail .= "Lenguaje: ".$user->leng."\n";
	$socio = $_POST['socioT']; // si es socio guardar la entidad	
	   
	if ($socio == "si")
    {
		$user->socio = $_POST['socio'];
	} 
	else
    {
		$user->socio = 0;
	}    
		$mail .= "Socio: ".$user->socio."\n";
		
	$seg = new Seguridad;
    $user->codigo = $seg->generaCadena(80); 
    
    //corregir por aqui
    if (!isset($_SESSION["pago"]))
    {        
        if (!isset($_POST['socio'])) 
        {
           $user->idPago = 0 ; 
        } 
        else 
        {
            $user->idPago = $_POST['socio'] ; 
        }
        
    }
     else
    {        
        $user->idPago = $_SESSION["pago"] ;                
    }
    
    /*echo "<br/> idpago: ".$user->idPago.
        "<br/> Seccion: ".$_SESSION["pago"].
        "<br/> Post:".$_POST["pago"];*/
    
	
    //registrar usuario
    if ($user->Nuevo())
    {    
      //echo "Pago post: ".$_POST['pago']." Seccion pago: ".$_SESSION["pago"];
	  if(($_POST['pago'] != "") Or  ($_SESSION["pago"] == 0))// pago individual
	  {
	  	//registro de medios de pago
      	$pagos = new Pagos; 	  	  
	  	$pagos->forma = $_POST['formasP'];
            $mail .= "Forma de Pago: ".$pagos->forma."\n";
	  	switch ($pagos->forma) 
	  	{
		  case 'efectivo':
			  $pagos->dato = "";
              $_SESSION["efectivo"] = "selected=\"selected\"";
		  break;
		  
		  case 'deposito':
			  $pagos->dato = $_POST['transaccion'];
              $_SESSION["deposito"] = "selected=\"selected\"";
		  break;
			  
		  case 'tarjeta':
			  $pagos->dato = $_POST['tarjetaP'];
              $_SESSION["tarjeta"] = "selected=\"selected\"";
		  break;	  
	  	}        
         
          $pagos->importe = $_POST['importe'];
                $mail .= "Importe: ".$pagos->importe."\n";
          $_SESSION["importe"] = $_POST['importe'];
		     
		  if($pagos->nuevo())
		  {	  			
				$pagos->campos = "idPago";
				$pagos->condicion = "forma='".$pagos->forma."' AND dato='".$pagos->dato.
								"' AND importe=".$pagos->importe." ORDER BY idPago";
				$aux = $pagos->consultaSQLbasicaRow();
                
				$_SESSION["pago"] = $aux["idPago"];
					$mail .= "IdPago: ".$aux["idPago"]."\n";
				$pagos->campos = "forma,dato,importe";   
				
				$pagoMje= "Pago registrado correctamente";                    
	  	  }
	  	  else
	  	 {
			$pagoMje= "Pago no registrado, por favor envie un mail a --";  
	  	 }	  		
	  	
	  	   
	  	   //registro del pago en la tabla usuarios
	  	 $user->campos = "idUsuario";
         $user->condicion = "doc='".$user->nroDoc."' AND tipoDoc='".$user->tipoDoc
                         ."' ORDER BY idUsuario DESC"; 
         $id = $user->consultaSQLbasicaRow(); 
         $_SESSION["idUsuario"] = $id["idUsuario"];
         $mje = "Usuario ".$id["idUsuario"]." registrado correctamente";          
         $user->campos = "idPago"; 
         $user->valores = $_SESSION["pago"];
         $user->condicion = "idUsuario=".$id["idUsuario"];
         $user->modificarSQL();
		 }
		 //pago grupal
		 
         if ((!isset($_POST['incripcionG'])) Or ($_POST['incripcionG']=="si")) 
         {
         	$_POST['incripcionG']="si";
         	if(!isset($_POST['cant']))
			{
				$_SESSION["personas"] = $_SESSION["personas"]- 1;
			}
			else
			{
				$_SESSION["personas"] = $_POST['cant'];
			}             
                        	 
         }   
       	else
       	{       	    
        	  session_destroy(); // si no es grupal, no sirve conservar la session 
       	}   
       
      
	   $ponecias = new Ponencia;
	   $ponecias->idUser = $_SESSION["idUsuario"];
       
	   for ($i=1; $i <3 ; $i++) 
	   {
	   		$ponecias->ponencia = $_POST['ponencia'.$i];
	   			$mail .= "Ponencia: ".$ponecias->ponencia."\n";
                
	   		$ponecias->eje = $_POST['ejes'.$i];	   
	   			$mail .= "Eje: ".$ponecias->eje."\n";	
                
            $ponecias->contendido = $_POST['contenidos'.$i];
                $mail .= "Contenido: ".$ponecias->contendido."\n";
            
			$ponecias->archivo = $_FILES['ponencia'.$i."F"]["name"];
				$mail .= "Archivo: ".$ponecias->archivo."\n";            
            
                
			$arch = $_FILES['ponencia'.$i."F"]["name"];   
                     
            $dir = "upload/arch".$_SESSION["idUsuario"]."_".$arch;
            $tipos = array("application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            if(in_array($_FILES['ponencia'.$i."F"]['type'],$tipos))
            {          
                if(copy($_FILES['ponencia'.$i."F"]['tmp_name'], $dir))
                {
                      $ponenciaMje .= "<br/><p class='text-success'>El archivo ".
                                    basename( $_FILES['ponencia'.$i."F"]['name']).
                                    " ha sido subido </p>";
                      if (($ponecias->ponencia != "") And
                            ($ponecias->archivo) != "")
                        {               
                            if ($ponecias->nuevo())
                            {
                                $ponenciaMje .= "<p>Ponencia:".$_POST['ponencia'.$i].
                                               " cargada correctamente </p>";       
                            }
                            else
                            {
                            	if($ponecias->ponencia !="")
                            	{                            	
									$ponenciaMje .= "<p class='text-danger'>Ponencia:".$_POST['ponencia'.$i].
                                               " no cargada correctamente </p>
                                            <p>Intentelo de nuevo por favor</p>";
								}
                            
                            }               
                        }
                }
                else
                {
                    $ponenciaMje .= "<br/><p class='text-danger'>
                                        ¡Ha ocurrido un error, trate de nuevo!
                                      </p>";
                }
             }
            else 
            {
                $ponenciaMje .= "<p class='text-danger'>Archivo: ".
                				basename( $_FILES['ponencia'.$i."F"]['name']).
                                " en formato no permitido no permitido</p>";   
            }            
			   
	   }
    } 
    else
    {
      $mje = "<p class='text-danger'>Fracaso en el registro de usuario</p>";  
    }
	
	
}
else
{
	echo "entro por donde no devia";
}

 if ($_REQUEST['leng']=="port")
 {
     $data = array( "titulo" => "Ficha de inscrição");
 }
 else
 {
     $data = array( "titulo" => "Formulario de Inscripción");
 }
?>

<!doctype html>
<html lang="es">
<head>
    <title><?php echo $data["titulo"]?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <hr />
        <div class="text-center">
            <h1><?php echo $data["titulo"]?></h1>
        </div>
        <?php echo "<p>".$mje."</p> <br/>"
                   ."<p>".$pagoMje."</p>".
                        $ponenciaMje
                   ."<p> Por favor revise su casilla de mail para la verificación y confirmacion del mismo </p>
                     <p>Dudas o consulas puede hacerlas al siguiente mail: 
                     <a href='mailto:congreso@ulapsi.org'>congreso@ulapsi.org</a></p><br/>";
					 
                     //echo " Personas: ".$_SESSION["personas"]." inscripcion Grupal: ".$_POST['incripcionG'];
                     if (($_SESSION["personas"] > 0) And ($_POST['incripcionG']=="si")) 
                     {
                         echo "<p>Ud realizo un registro grupal, aun quedan ".
                         $_SESSION["personas"]." personas a inscribir, si desea
                         hacerlo, puede hacer <a href='formulario.php?gpl=1'> click aqui</a> 
                         </p>";
                     }
                      else
                     {
                         echo "<p> Si desea realizar una nueva inscripcion puede
                               hacer <a href='formulario.php'>clic aqui</a></p>";
                     }
                    
                     if (mail("mjosemolina@gmail.com", "Nueva inscripcion", $mail))
                     {
                         echo "Email Enviado Correctamente";
                     }
                      else
                     {
                         echo "Error en el envio de mail";
                     }
                          
                     ?>
    </div>
</body>
</html>


