<?php
/**********************************************
 *****Libreria para acceso a base de datos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 //version en php 5.3
 header("Content-Type: text/html; charset=UTF-8");
//clase para acceder a la base de datos
class SQL {
	//instancias
	public $tabla = "";
	public $campos = "";
	public $valores = "";
	public $condicion = "";
	public $opcion = 0;
	public $estilo = "";
	public $id = "";
	public $muestra = "";
	public $predeterminado = "";
    public $add = "";

	// conecta a la base de datos
	function conectarbase() {
		/*$dbhost = "localhost";
		$dbusername = "root";
		$dbpass = "";
		$dbname = "incripcion";*/
		
        $dbhost = "localhost";
        $dbusername = "fepraadm_ariel";
        $dbpass = "Eracles1092";
        $dbname = "fepraadm_inscripcion";
        
		// conectar con el servidor
		$conexion = mysql_connect($dbhost, $dbusername, $dbpass) 
			or die("No se puede conectar al servidor");
	    
	    /*if (!$conexion)
			die("Falló la conexión a MySQL: " . mysql_error());
		else
			echo "Conexión exitosa!";*/
		
		// Seleccionar la bd
		mysql_select_db($dbname) 
			or die("No se puede seleccionar la base de datos");
		mysql_set_charset('utf8'); 
		
		return $conexion;
	}

	//Realiza una consulta basica
	//$conexion, $tabla, $campos, $condicion
	function consulSQLbasica() 
	{
		if ($this -> condicion == "") 
		{
			$query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . ";";
		}
		 else
	    {
			$query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
		}	
		$t = (strlen($query)-6)* -1;/* Esto es por poblemas de codificacion*/
		$query= substr(utf8_decode($query),$t);
		//echo  $query."<br/>";  //echo $query." ".$t;
		$consulta = mysql_query($query, $this -> conectarbase()) 
					or die("Fallo en la consulta");
		return $consulta;
	}

	// Realiza una consulta basica y devuelve el array
	function consultaSQLbasicaRow() 
	{
		$consulta = $this -> consulSQLbasica();
		$row = mysql_fetch_array($consulta); 
		return $row;
	}

	// Realiza la insercion de datos
	//$conexion,$tabla, $campos, $valores
	function insertaSQL() 
	{
		$query = "INSERT INTO ".$this -> tabla." (".$this -> campos .") VALUES (".$this -> valores.");";
		//echo  $query." <br />";
		$consulta = mysql_query($query, $this -> conectarbase());
		return $consulta;
	}

	// Realiza la modificacion de datos
	//$conexion, $tabla, $campos, $valores, $condicion
	function modificarSQL() 
	{
		$query = "UPDATE " . $this -> tabla . " SET ";
		$campos = utf8_decode($this -> campos);
		$atributos = explode(",", $campos);
		$val = explode(",", $this -> valores);
		$j = 0;
		foreach ($atributos as $i) 
		{
			$query .= "$i = $val[$j],";
			$j++;
		}
		$query = substr($query, 0, strlen($query) - 1);
		if ($this -> condicion != "") {
			$query .= " WHERE ";
			$query .= $this -> condicion;
		}
		$query .= ";";
		//echo $query;
		$consulta = mysql_query($query, $this -> conectarbase()) 
		          or die("Fallo en la modificacion");
		return $consulta;
	}

	//Realiza el borrado de datos de una base de datos
	//$conexion, $tabla, $condicion
	function BorraSQL() 
	{
		$query = "DELETE FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
		$consulta = mysql_query($query, $this -> conectarbase()) or die("Fallo la consulta");
		//echo $query;
		return $consulta;
	}

	// Muestra un select de una tabla predeterminada, devolviendo el valor
	//$conexion, $tabla, $id, $muestra, $condicion, $predeterminado, $estilo
	function selectTabla()
	 {
		$devuelve = "<select name='" .$this -> tabla.$this -> add."' class='" . $this -> estilo . "'>\n";
		$consulta = $this ->consulSQLbasica();
		$aux = explode(",", $this -> muestra);

		$row = mysql_fetch_array($consulta);
		while ($row != null) {
			if ($row[$this->id] == $this -> predeterminado) 
			{
				$devuelve .= "<option value='" . $row[$this -> id] . "' selected='selected'>";
				foreach ($aux as $i) 
				{
					$devuelve .= $row[$this->muestra];
				}
				$devuelve .= "</option>\n";
			} 
			else 
			{
				$devuelve .= "<option value='" . $row[$this -> id] . "'>";
				foreach ($aux as $i)
				{
					$devuelve .= $row[$this -> muestra];
				}
				$devuelve .= "</option>\n";
			}
			$row = mysql_fetch_array($consulta);
		}
		$devuelve .= "</select>\n";
		if ($this -> opcion == 0) 
		{
			return $devuelve;
		} 
		else 
		{		
			echo $devuelve;
		}

	}

}
?>