﻿<?php
/*************************************
 *****Sistema Escuela Zorrilla********
 *Sistema de manejo y administracion 
 *de Netbook***********************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
*Sitio Web: http://www.infrasoft.com.ar 
****************************************/ 

// librerias requeridas
//sql.php

// clase creada para la seguridad del sistema
class Seguridad extends SQL{
	//instacias
	public $user="";
	public $pass="";
	public $verif="";
	public $verifica="";
	public $logueado=FALSE;
	public $intentos =0;	
	public $tiempo=2000;
	public $hora = "";
	private $permitidos = ""; 
	
	public function  __construct()
	{
		$this->tabla = "user";
        $this->campos = "usuario,pass,verif";        	
	}
	
	//verifica si el login es correcto
	public function login($user,$pass)
	{
        $pass = $this->encriptar($pass);
		$this->condicion ="usuario='$user' AND pass='$pass'";	
        $row = $this->consultaSQLbasicaRow();
        //echo $pass." ".$this->encriptar($pass);
        if(is_array($row))
        {            
            $this->user = $row["usuario"];
			$this->pass = $row["pass"];
			$this->verif = $this->encriptar($row["verif"]);
			$this->verifica = $this->encriptar($row["usuario"]).
							  $this->encriptar($row["pass"]).
							  $row["verif"];
			$this->logueado = TRUE;
			$this->hora=time();
			//echo $this->verifica;
        }
        else
        {
            $this->intentos = $this->intentos + 1;
        }
	}
	
	//verifica la actividad del usuario, si esta o no frente al sistema
	public function verifica()
	{		
		if (($this->logueado == 1) Or
			(($this->encriptar($this->user) + 
			$this->encriptar($this->pass) +
			$this->encriptar($this->verif))== $this->verifica)) 
		{
			$this->hora = time();
			
		}
		 else
	    {
	    	if (($this->hora + $this->tiempo)>= time())
			{
				$this->hora = time();
			}
			 else
			 {
				$this->user = "";
				$this->pass = "";
				$this->verif = "";
				$this->verifica = "";
				$this->logueado = FALSE;
			}
			header('Location: login.php');			
		}
		//$this->Imprime();
	}
	
	// muestra todos los datos del objeto
	public function Imprime()
	{
		echo "Usuario: ".$this->user."<br/>".
			 "Password: ".$this->pass."<br/>".
			 "Verif: ".$this->verif."<br/>".
			 "Verificacion: ".$this->verifica."<br/>".
			 "Logueado: ".$this->logueado."<br/>";
	}		 
	//verifica que los datos ingresados no sea maliciosos
	public function texto($var)
	{
		return filter_var($var,FILTER_SANITIZE_EMAIL);
	}	
	
	// verifica que el ingreso solo sean numeros
	public function numeros()
	{
		
	}
	
	// genera un vector de numeros aleatorios, sin repetir 
    function generaAleatios($cantidad, $tope)
    {
        $aux[] = null;
        $num = rand(1,$tope);
        for ($i=0; $i < $cantidad; $i++)
        {
            if(in_array($num, $aux))
            {
                $i--;
            }
            else
            {
               $aux[$i] =  $num;
            }
            $num = rand(1,$tope);
        }               
        return $aux;        
    }
    
     //Genera una cadena aleatoria para confirmar seccion
    public function generaCadena($num)
    {
        $cad = '';
        $codigo = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($i=0; $i< $num;$i++)
        {
			$pos = rand(1,55);            
            $cad .=  $codigo[$pos];
        }        
        return $cad;
    }
	
	//encripta los numeros de una manera unica
	function encriptar($var)
	{
		$n = strlen($var);
		$codigo = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';		
		$encrip = sha1(crypt($var,$codigo));
		return $encrip;
	}
}

?>