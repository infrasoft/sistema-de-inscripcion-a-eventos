<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 //librerias requeridas: sql
 
 /**
  *  Clase de pagos del usuario
  */
 class Pagos extends SQL
 {
     public $idPago = 0;    
     public $forma = "";
     public $dato = "";
     public $importe = 0;
     
     function __construct() 
     {
        $this->tabla = "pago"; 
        $this->campos = "forma,dato,importe";    
     }
     
     //ingresa un nuevo metodo de pago
     public function nuevo()
     {
         $this->valores = "'".$this->forma."','".$this->dato."',".$this->importe;
         return $this->insertaSQL();
     }
     
     //realiza una consulta de un pago realizado
     public function consulta($idPago)
     {        
         $this->condicion = "idPago=".$idPago;
         return consultaSQLbasicaRow();
     }
 }
 
 
 ?>