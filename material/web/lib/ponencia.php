<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 //librerias requeridas: sql
 
 /**
  *  Clase para las ponencias de los usuarios
  */
 class Ponencia extends SQL 
 {
     public $idPonencia = 0;
     public $idUser = 0;
     public $ponencia = "";
     public $eje = 0;
     public $archivo = "";
     function __construct()
	 {
         $this->tabla = "ponencia";
		 $this->campos = "idUser,ponencia,eje,archivo";
     }
	 
	 //registra una nueva ponencia
	 public function nuevo()
	 {
		 $this->valores = $this->idUser.",'".$this->ponencia."',".
		                  $this->eje.",'".$this->archivo."'";
		 return $this->insertaSQL();
	 }
 }
 
 ?>