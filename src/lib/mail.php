<?php
/**********************************************
 ***** Libreria de mail- Inscripcion a eventos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 //librerias requeridas: sql, usuarios
 
 /**
  * Clase para optimizar el envio de mail
  */
 class email extends Usuarios
 {
 	public $headers = "";
  	public $to = "";   
	public $subject ="";
	public $mensaje ="";
	public $user = 0;
     function __construct()
	  {
          // Cabeceras del mail (Content-Type y demas info)
        $this->tabla = "usuarios";
		$this->campos = "apellido,nombre,tipoDoc,doc,pais,provincia,cp,direccion"
     	                .",telefono,email,categoria,leng,socio,verificado,"
     	                ."presente,emitido,codigo,idPago";  
    	$this->headers = "MIME-Version: 1.0\n";
    	$this->headers .= "Content-type: text/html; charset=utf-8\n";
     }
	  
	 //envia el mail para la confirmacion de datos 
	 public function comprobar($link)
	 {
	 	$row = $this->consulta($this->user);
	 	$this->to = $row["email"];
	 	$this->headers .= "From: FEDERACION DE PSICOLOGOS DE LA REPUBLICA ARGENTINA <mjosemolina@gmail.com>\n";   
        $this->headers .= "Reply-To: congreso@ulapsi.org\n";
	 	$this->subject = "Confirmacion de Email";
		$this->mensaje = "<p>Estimado/a ".$row["apellido"].", ".$row["nombre"].":</p> <br />
 
 <p>Acabas de inscribirte en el evento de la confederacion Argentina de psicologos, para confirmar tus datos, 
 	por favor tiene que realizar un 
 	<a href='http://www.fepra.org.ar/2016Congreso/admin/usuarios.php?".$link."&id=".$this->user."'> aqui</a>
 </p>
 
 <p>http://www.fepra.org.ar/2016Congreso/admin/usuarios.php?".$link."&id=".$this->user."</p>
 
 <p>En caso de no funcionar. Por favor copie y pegue el siguiente link en la barra de navegacion</p>	
 	
 <p>En caso de no haber solicitado dicha inscripcion, por favor ignore este mail</p>
 
 <p>
 	Un abrazo cordial
 	
 </p>
 
  <div align='center'>
  	<p>gracias por participar</p>
  	
  </div>";
  	//echo "Enviando email:".$this->to." ".$this->subject." ".$this->mensaje." ".$this->headers;
		return mail($this->to, $this->subject, $this->mensaje, $this->headers);
	 } 
     
     //envia los mail los certificados solicitados
     public function certificados()
     {
     	$row = $this->consulta($this->user);
	 	$this->to = $row["email"];     	
         $this->subject = "Impresión de certificados";	
         $this->mensaje= "<p>Estimado/a ".$row["apellido"].",".$row["nombre"]."<p>

                       <p>Has solicitado que se te envié los certificados a tu
                        bandeja de correo, por lo tanto solo tienes que hacer
                         clic en el siguiente enlace:</p>
                         
<a href='http://www.fepra.org.ar/2016Congreso/admin/pdf.php?c=".$row["codigo"]."&id=".$this->user."&o=3'>
	http://www.fepra.org.ar/2016Congreso/admin/pdf.php?c=".$row["codigo"]."&id=".$this->user."&o=3
</a></br>

<p>Si no lo puedes hacer, simplemente tienes que copiarlo y pegarlo en tu navegador</p>
<p>Saludos cordiales</p>
<div align='center'>
  	<p>gracias por participar</p>
  	
  </div>
         ";
         return mail($this->to, $this->subject, $this->mensaje, $this->headers);
     }
     
     
 }
 
 
 ?>
 
 	
