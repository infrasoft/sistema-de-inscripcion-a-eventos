<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 //librerias requeridas: sql
 
 /**
  * Clase para el manejo de Ejes
  */
 class Ejes extends SQL 
 {  
     
     function __construct()
     {
         $this->tabla = "ejes";
         $this->campos = "id,esp,port";
         $this->muestra = "esp";
     }
     
     public function selectEjes()
     {
         $this->estilo = "form-control";
         $this->id = "id";         
         return $this->selectTabla(); 
     }
 }
     
?>