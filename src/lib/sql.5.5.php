<?php
/**********************************************
 *****Libreria para acceso a base de datos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 //version en php 5.5 - usando mysqli
 

 
 //header("Content-Type: text/html; charset=UTF-8");
//clase para acceder a la base de datos
class SQL {
	//instancias
	public $tabla = "";
	public $campos = "";
	public $valores = "";
	public $condicion = "";
	public $opcion = 0;
	public $estilo = "";
	public $id = "";
	public $muestra = "";
	public $predeterminado = "";
    public $add = "";
	private $dbhost = "localhost";
   private $dbusername = "root";
    private $dbpass = "";
    private $dbname = "incripcion";
	/* private $dbusername = "fepraadm_ariel";
	private $dbpass = "Eracles1092";
	private	$dbname = "fepraadm_inscripcion";*/

	// conecta a la base de datos
	function __construct()
	{
		mysql_set_charset('utf8'); 
	}
	
	//conecta la base de datos
	public function conecta()
	{
		$mysqli = new mysqli($this->dbhost, $this->dbusername, $this->dbpass, $this->dbname); 
	 	if($mysqli->connect_errno > 0)
	 	{  	 
    		  die("Imposible conectarse con la base de datos [" . $mysqli->connect_error . "]");   
 	 	}
		return $mysqli;
	}
	//Realiza una consulta basica
	//$conexion, $tabla, $campos, $condicion
	function consulSQLbasica() 
	{
		if ($this -> condicion == "") 
		{
			$query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . ";";
		}
		 else
	    {
			$query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
		}	
		$t = (strlen($query)-6)* -1;/* Esto es por poblemas de codificacion*/
		$query= substr(utf8_decode($query),$t);
		
		//echo  $query."<br/>";  //echo $query." ".$t;
		
		$mysqli = $this->conecta();	 	
		$consulta = $mysqli->query($query); 
		if($mysqli->errno) die($mysqli->error); 			
		return $consulta;
	}

	// Realiza una consulta basica y devuelve el array
	function consultaSQLbasicaRow() 
	{
		$consulta = $this -> consulSQLbasica();
		$row = $consulta->fetch_assoc(); 
		return $row;
	}

	// Realiza la insercion de datos
	//$conexion,$tabla, $campos, $valores
	function insertaSQL() 
	{
		$query = "INSERT INTO ".$this -> tabla." (".$this -> campos .") VALUES (".$this -> valores.");";
		//echo  $query." <br />";
		$mysqli = $this->conecta();
		$consulta = $mysqli->query($query);
		if($mysqli->errno) die($mysqli->error); 
		return $consulta;
	}

	// Realiza la modificacion de datos
	//$conexion, $tabla, $campos, $valores, $condicion
	function modificarSQL() 
	{
		$query = "UPDATE " . $this -> tabla . " SET ";
		$campos = utf8_decode($this -> campos);
		$atributos = explode(",", $campos);
		$val = explode(",", $this -> valores);
		$j = 0;
		foreach ($atributos as $i) 
		{
			$query .= "$i = $val[$j],";
			$j++;
		}
		
		$query = substr($query, 0, strlen($query) - 1);
		
		if ($this -> condicion != "") {
			$query .= " WHERE ";
			$query .= $this -> condicion;
		}
		$query .= ";";
		//echo $query;
		$mysqli = $this->conecta();
		$consulta = $mysqli->query($query);
		if($mysqli->errno) die($mysqli->error); 
		return $consulta;
	}

	//Realiza el borrado de datos de una base de datos
	//$conexion, $tabla, $condicion
	function BorraSQL() 
	{
		$query = "DELETE FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
		//echo $query;
		$mysqli = $this->conecta();
		$consulta = $mysqli->query($query);
		if($mysqli->errno) die($mysqli->error); 		
		return $consulta;
	}

	// Muestra un select de una tabla predeterminada, devolviendo el valor
	//$conexion, $tabla, $id, $muestra, $condicion, $predeterminado, $estilo
	function selectTabla()
	 {
		$devuelve = "<select name='" .$this -> tabla.$this -> add."' class='" . $this -> estilo . "'>\n";
		$consulta = $this ->consulSQLbasica();
		$aux = explode(",", $this -> muestra);

		$row = $consulta->fetch_assoc();
		while ($row != null) {
			if ($row[$this->id] == $this -> predeterminado) 
			{
				$devuelve .= "<option value='" . $row[$this -> id] . "' selected='selected'>";
				foreach ($aux as $i) 
				{
					$devuelve .= utf8_encode($row[$this->muestra]);
				}
				$devuelve .= "</option>\n";
			} 
			else 
			{
				$devuelve .= "<option value='" . $row[$this -> id] . "'>";
				foreach ($aux as $i)
				{
					$devuelve .= utf8_encode($row[$this -> muestra]);
				}
				$devuelve .= "</option>\n";
			}
			$row = $consulta->fetch_assoc();
		}
		$devuelve .= "</select>\n";
		if ($this -> opcion == 0) 
		{
			return $devuelve;
		} 
		else 
		{		
			echo $devuelve;
		}
	}
	
	
}
?>