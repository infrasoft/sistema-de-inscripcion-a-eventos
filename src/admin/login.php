<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
 //librerias requeridas sql, seguridad
 include '../lib/sql.5.5.php';
 include '../lib/seguridad.php';
 
 //inicializar variables
 $mje="";
 if (!isset($_REQUEST["login"]))
 {
     $op = "";
 }
 else
 {
     $op = $_REQUEST["login"]; 
 }
 
$_SESSION['seguridad'] = new Seguridad;
 
 ?>
  <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <hr />
        <?php 
        	switch ($op)
			 {
				case 'login':
					$_SESSION['seguridad']->user = $_REQUEST["user"];
					$_SESSION['seguridad']->pass = $_REQUEST["pass"];
					if($_SESSION['seguridad']->login())
                    {
                    	
                        echo "<h2>
                        		<a href='admin.php' class='text-primary'>Ingreso al sitio</a>
                        	 </h2>";
							/* if (is_object($_SESSION['seguridad'])) 
							 {
								echo "es un objeto";	 
							 }*/
                    }
                    else 
                    {
                        echo "<h1> Login</h1>
        <div class='text-center'>
            <p class='text-danger'> Usuario o password incorrectos</p>
            <p>Por favor Ingrese su usuario y password</p>
        </div>
        
        
        <form class='form-inline' role='form' action='login.php' method='post'>
            <div class='text-center'>
                <label>Usuario:</label>
                <input type='text' class='form-control' name='user'/>
                <label>Password:</label>
                <input type='password' class='form-control' name='pass'/>
            </div>
            <div class='text-center'>        
                <button type='submit' class='btn btn-info' name='login' id='login' value='login'>Enviar</button>
                <button type='reset' class='btn btn-default'>Cancelar</button>      
            </div>
        </form>";
                    }
					break;
				
				default:
					echo "<h1> Login </h1>
        <div class='text-center'>
        	<p>Por favor Ingrese su usuario y password</p>
        </div>
        
        
        <form class='form-inline' role='form' action='login.php' method='post'>
        	<div class='text-center'>
        		<label>Usuario:</label>
        		<input type='text' class='form-control' name='user'/>
        		<label>Password:</label>
        		<input type='password' class='form-control' name='pass'/>
        	</div>
        	<div class='text-center'>        
            	<button type='submit' class='btn btn-info' name='login' id='login' value='login'>Enviar</button>
                <button type='reset' class='btn btn-default'>Cancelar</button>		
			</div>
        </form>";
					break;
			}
        ?>
        
        
<div class="text-center">
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>