<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
 //librerias requeridas
 include '../lib/sql.5.5.php';
 include '../lib/pago.php';
 include '../lib/usuarios.php';
 include '../lib/ponencia.php';
 include '../lib/seguridad.php';
  
 //inicializar variables 
  
 
if (!isset($_REQUEST["id"]))
{
	$id=0;
	header('Location: logout.php');
}
else
{
	$id = $_REQUEST["id"];
	$user = new Usuarios;
	$row = $user->consulta($id);
	if (isset($_REQUEST["c"]) And ($row["codigo"] == $_REQUEST["c"])) //codigo de seguridad
	{		
		//header('Location: admin.php');
	}
	 else
    {
		//header('Location: logout.php');
		$_SESSION['seguridad']->verifica();  
	}
	
}
 
 
define('FPDF_FONTPATH','../lib/font/');
require('../lib/fpdf.php');
class PDF extends FPDF
{
	public $apellido = "";
	public $nombre = "";
	public $dni = "";
	public $otro = "";
	public $op = 1;
	
	function ponencia()
	{
		$this->AddPage();
		
		switch ($this->op) 
		{
			case '0': // sin imagen de fondo
				
			break;			
			
			case '1': //con imagen de fondo
				$this->Image("../img/ponencia.jpg",'0','0','300','210','JPG');
			break;
				
			case '2'://con fondo y firma
				$this->Image("../img/ponencia1.jpg",'0','0','300','210','JPG');
			break;
		}							
			
		$this->Ln(70);
		$this->Cell(50,40,'','','','C','');
		$this->SetFont('Arial','B',20);
		$this->Cell(210,4,$this->apellido.', '.$this->nombre."; ".$this->dni,'','','C','');
		$this->Ln(20);
		$this->Cell(230,4,$this->otro,'','','R','');
	}
	
	public function usuarios()
	{
		$this->AddPage();
			
		switch ($this->op) 
		{
			case '0': // sin imagen de fondo
				
			break;			
			
			case '1': //con imagen de fondo
				$this->Image("../img/usuario.jpg",'0','0','300','210','JPG');
			break;
				
			case '2'://con fondo y firma
				$this->Image("../img/usuario1.jpg",'0','0','300','210','JPG');
			break;
		}
		$this->Ln(67);
		$this->Cell(40,40,'','','','C','');
		$this->SetFont('Arial','B',20);
		$this->Cell(230,4,$this->apellido.', '.$this->nombre."; ".$this->dni,'','','C','');
		$this->Ln(10);
		$this->Cell(180,4,$this->otro,'','','R','');
	}

}// fin clase



$pdf=new PDF("L"); //constructor pdf

if(isset($_REQUEST["o"]))
{
	$pdf->op=$_REQUEST["o"];
}



$pdf->apellido = $row["apellido"] ;
$pdf->nombre = $row["nombre"] ; 
$pdf->dni = $row["tipoDoc"].": ".$row["doc"];
$pdf->otro = strtoupper ($row["categoria"] ); 
$pdf->usuarios();

$pon = new Ponencia;
$pon->condicion = "idUser=".$id;
$consulta = $pon->consulSQLbasica();
while($row = $consulta->fetch_assoc())
{
	$pdf->otro = $row["ponencia"];
	$pdf->ponencia();
}

$pdf->Output();

$user->campos  = "emitido";
$user->valores = "'si'";
$user->modificarSQL();
