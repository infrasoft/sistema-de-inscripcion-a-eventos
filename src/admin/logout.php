<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
 //librerias requeridas
  include '../lib/sql.5.5.php';
 //include '../lib/seguridad.php';
 session_destroy();
 //$_SESSION['seguridad'] = new Seguridad;
 //echo "Pass ".$seg->encriptar("piaget4075").
 	//   " <br/> aleatorio:".$seg->generaCadena(50) 
 	
 //inicializar variables
 //echo "Comprobar:".$_SESSION['verifica'];
 
 ?>
 <html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <hr />
        
<div class="text-center">
        <h1> Fuera del sistema de inscripcion</h1>
        <br />
        <p> Le informamos al usuario  que se encuentra fuera del sistema. 
            La sección ha caducado o bien el usuario a salido del sistema</p><br />
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>