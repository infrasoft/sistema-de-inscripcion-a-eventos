<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
session_start();
if(!isset($_SESSION["verif"]))
{    
    header("Location: login.php");
}

 //librerias requeridas
include '../lib/sql.5.5.php';
include '../lib/ejes.php';
include '../lib/seguridad.php'; 
  
 //inicializar variables 
  
 $_SESSION['seguridad']->verifica();
  header('Content-Type: text/html; charset=UTF-8'); 

  $eje = new Ejes;
  $consulta = $eje->consulSQLbasica();
 
 ?>
   <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
    	<div class="text-center">
            <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        </div>
        <div class="text-right bold">
            <a href="logout.php"  class="text-danger"><b>Desconectarse</b></a>
        </div>
        <hr />
        
        <nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li ><a href="admin.php">Inscriptos</a></li>
      <li><a href="ponencia.php">Ponencias</a></li>
      <li><a href="pagos.php">Pagos</a></li>
      <li class="active"><a href="ejes.php">Ejes</a></li>
      <li><a href="contenidos.php">Contenidos</a></li>
    </ul> 
     
  </div>
  
</nav>

	<h1> Lista de ejes </h1>
	<div class="table-responsive">
	<table class="table table-striped">
		<tr>
			<td><b>Id</b></td>
			<td><b>Español</b></td>
			<td><b>Portugues</b></td>			
		</tr>
		<?php 
			while ($row = $consulta->fetch_assoc())
			{
				echo "<tr>
						  <td>".utf8_encode($row["id"]).
						 "</td>
						  <td>".utf8_encode($row["esp"]).
						  "</td>
						  <td>".utf8_encode($row["port"]).
						  "</td>
				    </tr>";	
			}
		?>
	</table>
	</div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>