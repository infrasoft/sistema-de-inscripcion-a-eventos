<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
 if(!isset($_SESSION["verif"]))
{    
    header("Location: login.php");
} 
 
 //librerias requeridas
include '../lib/sql.5.5.php';
include '../lib/usuarios.php';
include '../lib/pago.php';
include '../lib/ponencia.php';
include '../lib/seguridad.php'; 

//$_SESSION['seguridad']->Imprime();

 //inicializar variables 
 
 $_SESSION['seguridad']->verifica();
 
 if (!isset($_REQUEST["id"]))
 {
	$id = 0;
 }
 else
 {
     $id = $_REQUEST["id"];
 } 
 ?>
 <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>        
        <div class="text-right bold">
        	<a href="logout.php"  class="text-danger"><b>Desconectarse</b></a>
        </div>
        <hr />

<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li class="active"><a href="admin.php">Inscriptos</a></li>
      <li><a href="ponencia.php">Ponencias</a></li>
      <li><a href="pagos.php">Pagos</a></li>
      <li><a href="ejes.php">Ejes</a></li>
      <li><a href="contenidos.php">Contenidos</a></li>
    </ul>
 
    <form class="navbar-form navbar-left" role="search" action="admin.php">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar" name="buscar">
        <select class="form-control" name="campo">
        	<option value="apellido">apellido</option>
        	<option value="nombre">nombre</option>
        	<option value="doc">documento</option>
        	<option value="telefono">telefono</option>
        	<option value="email">email</option>
        	<option value="categoria">categoria</option>
        </select>
      </div>
      <button type="submit" class="btn btn-default">Buscar</button>
    </form>   
  </div>
  
</nav>

	<div class="text-center">
		<h1> Eliminar usuarios</h1>
	</div>
	
	<p class="text-primary">Se advierte al usuario que el proceso es irreversible, por favor corroborar bien los datos</p>
	
	<?php 
			$user = new Usuarios;
			$row = $user->consulta($id); // datos del usuario
		
		echo "<h2>Datos del usuarios</h2>
				<p>Apellidos: ".$row["apellido"]." Nombre: ".$row["apellido"]." Tipo de documento ".$row["tipoDoc"].
				" Nº: ".$row["doc"]."</p>";
			
		echo "<h2>Pagos realizados</h2>";			
			$pago = new Pagos;
			$rowPago = $pago->consulta($row["idPago"]);
			
		echo "<p>IdPago: ".$row["idPago"]." Forma: ".$rowPago["forma"]." Datos: ".$rowPago["dato"]." Importe: ".$rowPago["importe"]." </p>
			  <h2>Ponencias subidas</h2>";
			  
			  $ponenc = new Ponencia;
			  $ponenc->condicion = "idUser=".$id;
			  $ponenc->campos = "idPonencia,idUser,ponencia,eje,contendido,archivo";
			  $consulta = $ponenc->consulSQLbasica(); 
			  while ($rows = $consulta->fetch_assoc()) 
			  {
			  		echo "<p>IdPonencia: ".$rows["idPonencia"]." ponencia: ".
			  				$rows["ponencia"]." Eje:".$rows["eje"]." Contenido: ".
							$rows["contendido"]." Archivo: ".$rows["archivo"]."</p>";
			  }
			  
			  //proceso de eliminacion de registro
			  if (isset($_REQUEST["eliminar"])) 
			  {
				  $id = $_REQUEST["id"];
				  while ($rows = $consulta->fetch_assoc()) 
			  	 {
			  	 	$ponenc->idPonencia = $rows["idPonencia"];
			  	 	if($ponenc->eliminar())
			  	 	{
			  	 		echo "<p>Ponencia Eliminada</p>";
			  	 	}
			  	 	else 
			  	 	{
						echo "<p>Fallo en la eliminacion de ponencia</p>";   
					 }
			  	 }
			  	 
				  if($pago->elimina($row["idPago"]))
				  {
				  	echo "<p>Pagos registrados eliminados</p>";
				  }
				  else
				   {
			  		echo "<p>Fallo en la eliminacion de pagos</p>";
				  }
				   
				  if($user->elimina($id))
				  {
				  	echo "<p>Usuario Eliminado</p>";
				  }
				  else
				  {
					echo "<p>Fallo en la eliminacion del usuario</p>";  
				  } 
			  }
			  else
			  {
				echo "<form role='form' action='eliminar.php'>
						<input type='hidden' value='$id' name='id'/>
						<input type='hidden' name='idPago' value='' id='idPago'/>
						<input type='submit' name='eliminar' value='Eliminar Registro' class='btn btn-default'/>		
					</form > ";	  
			  }
	?>    
	  
	
    <div class="text-center">
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>
