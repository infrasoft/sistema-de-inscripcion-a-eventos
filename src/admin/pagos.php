<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 session_start();
if(!isset($_SESSION["verif"]))
{    
    header("Location: login.php");
} 
 
 //librerias requeridas
 include '../lib/sql.5.5.php';
 include '../lib/pago.php';
  include '../lib/seguridad.php'; 
  
 //inicializar variables 

 $_SESSION['seguridad']->verifica();
 $mje="";
 $pago = new Pagos; 
 $pago->campos = "idPago,".$pago->campos;
 if (!isset($_REQUEST["id"]))
 {
     $id = 0;
	 if (isset($_REQUEST["campo"])) 
	 {
		 $pago->condicion = $_REQUEST["campo"].
                        " LIKE '%".
                        $_REQUEST["buscar"]."%'";
      $mje = "Datos filtrados de la busqueda del dato <b>".
                $_REQUEST["buscar"]."</b> del campo <b>".
                $_REQUEST["campo"]."</b>";
	 }
     $consulta = $pago->consulSQLbasica();
	  $op = "lista";
	  
 }
  else
 {
     $id = $_REQUEST["id"];     
     $consulta = $pago->consulta($id);
	 if (isset($_REQUEST["op"]))
 	 {
    	 $op = $_REQUEST["op"];
 	 }
	 else
	 {
		 $op = "";
	 } 
 }
 
 ?>
 <!doctype html>
<html lang="es">
<head>
    <title>Inscripcion a Eventos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
    <div class="container">
        <img src="../img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
        <div class="text-right bold">
            <a href="logout.php"  class="text-danger"><b>Desconectarse</b></a>
        </div>
        <hr />
        
        

<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li ><a href="admin.php">Inscriptos</a></li>
      <li><a href="ponencia.php">Ponencias</a></li>
      <li class="active"><a href="pagos.php">Pagos</a></li>
      <li><a href="ejes.php">Ejes</a></li>
      <li><a href="contenidos.php">Contenidos</a></li>
    </ul>
 
    <form class="navbar-form navbar-left" role="search" action="pagos.php">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar" name="buscar">
        <select class="form-control" name="campo">
            <option value="idPago">idPago</option>
            <option value="forma">Forma</option>
            <option value="dato">Datos</option>
            <option value="importe">Importe</option>            
        </select>
      </div>
      <button type="submit" class="btn btn-default">Buscar</button>
    </form>
    
    
  </div>
</nav>

	<?php
		switch ($op)
		{
			case 'lista':
	echo	"<div class='text-center'>
        	<h1> Lista de Pagos realizados</h1>
    	</div>
    
    <p class='text-primary'> $mje</p>
    
    <div class=\"table-responsive\">
    <table class=\"table table-striped\">
        <tr>
            <td><b>Id Pago</b></td>
            <td><b>Forma</b></td>
            <td><b>Datos</b></td>
            <td><b>Importe</b></td>
            <td><b>Opciones</b></td>            
        </tr>";
		$acum = 0;
        while($row = $consulta->fetch_assoc())
        {
           echo "<tr>
                    <td>".$row['idPago'].
                    "</td>
                    <td>".$row['forma'].
                    "</td>
                    <td>".$row['dato'].
                    "</td>
                    <td>".$row['importe'].
                    "</td>
                    <td>
                    	<a href='pagos.php?id=".$row['idPago']."&op=mdf' title='Modificar datos'>
                    		<span class='glyphicon glyphicon-pushpin'> </span>
                    	</a>
                    	<a href='admin.php?buscar=".$row['idPago']."&campo=idPago' title='Usuarios asociados al pago'>
                    		<span class='glyphicon glyphicon-sort'> </span>
                    	</a>
                    	<a href='pagos.php?id=".$row['idPago']."&op=elimina' title='Eliminar un pago'>
                    		<span class='glyphicon glyphicon-remove'> </span>
                    	</a>
                    </td>                   
                 </tr>";
		    $acum += $row['importe'];
        }
		echo "</table>        
    </div>
    <div class=\"text-center\">
    	<p>Total Acumulado: $".$acum.
		"</p>
    </div>";
				break;
				
			//modificar datos de pagos
			case 'mdf':
				$efe=""; $dep=""; $tarj="";
				switch ($consulta["forma"])
				 {
					case 'efectivo':
						$efe="selected=\"selected\"";
					break;
					case 'deposito':
						$dep="selected=\"selected\"";
					break;
					case 'tarjeta':
						$tarj="selected=\"selected\"";
					break;					
				}
			echo  "	<h1> Modificar datos de pago</h1>
    <form class='form-inline' role='form' action='pagos.php' method='post'>
    	<label>IdPago: ".$consulta["idPago"]."</label>
    	<input type='hidden' name='id' value='".$consulta["idPago"]."' id='idPago'/>
    	<label> Forma de pago:</label>
    	<select class='form-control' id='formasP' name='formasP'>
    		<option value='efectivo' ".$efe.">Efectivo</option>
    		<option value='deposito' ".$dep.">Deposito Bancario</option>
    		<option value='tarjeta' ".$tarj.">Tarjetas de credito</option>
    	</select>
    	<label>Datos:</label>
    	<input type='text'  name='dato' class='form-control' value='".$consulta["dato"]."' />
    	<label>Importe:</label>
    	<input type='text' name='importe' class='form-control' value='".$consulta["importe"]."' required />
    	<div class='text-center'>        
            <button type='submit' class='btn btn-info' name='op' id='op' value='modifica'>Modificar</button>
		    <button type='reset' class='btn btn-default'>Cancelar</button>		
		</div>
    </form>	";
			break;
			
			case 'modifica':
			
				echo "<h1> Modificar pagos</h1>";
				$pago->idPago = $_REQUEST["id"];
				$pago->forma = $_REQUEST["formasP"];
				$pago->dato = $_REQUEST["dato"];
				$pago->importe = $_REQUEST["importe"];
				if ($pago->modifica())
				 {
					echo "<p class='text-primary'> Datos modificados correctamente</p>";
				}
				 else
				  {
					echo "<p class='text-danger'>Fallos en la modificacion de datos</p>";
				}
				
				
			break;	
			
			case 'elimina':
				$pago->idPago = $_REQUEST["id"];
				if($pago->elimina($_REQUEST["id"]))
				{
					echo "<p>Pago eliminado correctamente</p>";	
				}
				else
				 {
					echo "<p>Fallo en la eliminacion de un pago</p>";
				}	
			break;
			
			default:
				
				break;
		}
	?>    
        
    
    <div class="text-center">
       <p class="text-muted">
           <small>
               Sistema de inscripcion desarrollado por 
                <a href="http://infrasoft.com.ar"> 
                    Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
                </a>      
               - © Derechos reservados
          </small>
         </p>
     </div>  
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../js/responsive.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/form.js"></script>
</body>
</html>
