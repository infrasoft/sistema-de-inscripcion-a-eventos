<?php
/**********************************************
 ***** Sistema de inscripcion a eventos *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 * Direccion Alvarado 1073. Local 3
 ****************************************/
 
 //librerias incluidas
include  './lib/sql.5.5.php';
include './lib/socios.php';
include './lib/ejes.php';
include "./lib/contenidos.php";
 
 session_start();

 
 if (!isset($_SESSION["importe"])) 
 {
     $_SESSION["importe"] = ""; // si
     $_SESSION["efectivo"] = "";// si
     $_SESSION["deposito"] = "";// si
     $_SESSION["tarjeta"] = "";// si
     $_SESSION["idUsuario"] = "";
	 $_SESSION["personas"] = "";// si
	 $_SESSION["pago"] = 0; // idPago
 }
 
 	//echo "datos: importe ".$_SESSION["importe"]." efectivo".$_SESSION["efectivo"]." ".$_SESSION["deposito"]." ".$_SESSION["tarjeta"]." ".$_SESSION["idUsuario"]." ".$_SESSION["personas"]." ".$_SESSION["pago"];

 
 if (isset($_REQUEST["gpl"]))
 { // inscripcion grupal
     $gpl = array("estado"=>"disabled",//"",
     			   "bloquear" => "disabled='disabled'",
                  "importe" => $_SESSION["importe"],
                  "efectivo" => $_SESSION["efectivo"],
                  "deposito" => $_SESSION["deposito"],
                  "tarjeta" => $_SESSION["tarjeta"],
                  "idUsuario" => $_SESSION["idUsuario"],
                  "personas" => $_SESSION["personas"], // cantidad de personas
                  "chek1" => "checked",
                  "chek2" => "",
                  "pago" => $_SESSION["pago"]
                  );     
 }
  else 
 { //inscripcion individual
 	$gpl = array("estado"=>"",
 				 "bloquear" =>"",
                 "importe" => $_SESSION["importe"],
                  "efectivo" => "selected=\"selected\"",
                  "deposito" => "",
                  "tarjeta" => "",
                  "idUsuario" => "",
                  "personas" => "",
                  "chek1" => "",
                  "chek2" => "checked",
                  "pago" => ""
                  );  
     session_destroy();
 }
 
 
 if (isset($_REQUEST['leng'])) //aqui se seleccionara el idioma del participante
 {//portugues
 	$leng = "port";
     $data = array( "nombre"=>"Nome  (*)",
 				"apellido" => "Sobrenome (*) ",
                "tipoDoc" =>"Tipo de documento: ",
                "nroDoc" => "Número do Documento (*) ",
                "pais" => "Pais ",
                "provincia" => "Provincia",
                "ciudad" => "Cidade (*) ",
                "direccion" => "Direccion (*) ",
                "cp" => "Código postal (*) ",
                "telefono" => "Telefone (*) ",
                "correo" => "e-mail (*) ",
                "socio" => "Associado ",
                "formasP" => "Método de pago ",
                	"efectivo" => "Espécie ",
                	"deposito"=> "Depósito bancário",
                	"tarjeta" => "Cartão Credito  ",
                "transaccion" => "Nr. Transação ",
                "tarjetaP" => "Cantidad de Cuotas",
                "importe" => "Valor (*)",
                "incripcionG" => "inscripción Grupal ",
                "cant" => "Número de pessoas para se registrar",
                "categoria" => "Categoria ",
                	"autor"=> "Autor",
                	"coautor" => "Co-Autor",
                	"participante" => "Participante",
                	"conferencista" => "Conferencista",
                	"estudiante" => "Estudante",
                	"expositor" => "Expositor Convitado",
                "entidad" => "Entidad ",
                "ponencia1" => "Trabalho 1 ",
                "ponencia2" => "Trabalho 2 ",
                "ponencia3" => "Trabalho 3 ",
                "mensaje" =>"Por favor preencha o seguinte formulário para inscrever-se no: VI Congreso ULAPSI.
								Se tiver qualquer duvida ou comentário por favor escreva ao  congreso@ulapsi.org",
                "datosObli" => "Algunos campos filho obligatorios (*):",
                "idioma"=>"Cambio de idioma",
				"titulo" => "Ficha de inscrição");
 } 
 else
 {		//español
 	 $leng = "esp";
     $data = array( "nombre"=>"Nombre: (*) ",
 				"apellido" => "Apellido (*) ",
                "tipoDoc" =>"Tipo de documento: ",
                "nroDoc" => "Numero de Documento (*) ",
                "pais" => "Pais ",
                "provincia" => "Provincia ",
                "ciudad" => "Ciudad (*) ",
                "direccion" => "Direccion (*) ",
                "cp" => "Codigo Postal (*) ",
                "telefono" => "Telefono (*) ",
                "correo" => "Correo Electonico (*) ",
                "socio" => "Socio ",
                "formasP" => "Formas de Pago ",
                	"efectivo" => "Efectivo",
                	"deposito"=> "Deposito Bancario",
                	"tarjeta" => "Tarjetas de credito",
                "transaccion" => "Nro de Transaccion ",
                "tarjetaP" => "Cantidad de Cuotas ",
                "importe" => "Importe (*) ",
                "incripcionG" => "Inscripcion Grupal ",
                "cant" => "Cantidad de personas a inscribir",
                "categoria" => "Categoria ",
                	"autor"=> "Autor",
                	"coautor" => "Co-Autor",
                	"participante" => "Participante",
                	"conferencista" => "Conferencista",
                	"estudiante" => "Estudiante",
                	"expositor" => "Expositor Invitado",
                "entidad" => "Entidad ",
                "ponencia1" => "Ponencia 1  ",
                "ponencia2" => "Ponencia 2 ",
                "ponencia3" => "Ponencia 3 ",
                "mensaje" =>"Por favor complete el siguiente formulario de inscripcion para el VI Congreso ULAPSI.
                			 Dudas o consultas puede hacerlo al siguiente mail congreso@ulapsi.org",
                "datosObli" => "Algunos campos son obligatorios (*):",
                "idioma"=>"Cambio de idioma",
				"titulo" => "Formulario de Inscripción");
 }
 
 if (!isset($_SESSION["mje"])) 
 {
     $_SESSION["mje"]="";
 } 
 
 
 header("Content-Type: text/html; charset=UTF-8");
 
?>

<!doctype html>
<html lang="es">
<head>
	<title><?php echo $data["titulo"]?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="./css/styles.css" rel="stylesheet" media="screen"> 
</head>
<body>
	<div class="container">
		<img src="img/encabezado.png" alt="Logo del evento" class="img-rounded"/>
		<hr />
		
		<div class="btn-group text-center">			
			<button type="button" class="btn btn-default dropdown-toggle"
   	       data-toggle="dropdown">
   				<?php echo $data["idioma"]?> <span class="caret"></span>
  			</button>
 
  			<ul class="dropdown-menu" role="menu">
    			<li><a href="formulario.php">Español</a></li>
    			<li><a href="formulario.php?leng=port">Portugues</a></li> 
  			</ul>
		</div>		
		
	 <div class="text-center">
	     <h1><?php echo $data["titulo"]?></h1>
	 </div>
	 	
	 <p><?php echo $data["mensaje"]?></p>
	<p class=""> <?php echo $data["datosObli"]?> </p>
	<p class="text-danger"><?php echo $_SESSION["mje"]?></p>
	<!-- :::::::::::::::Formulario de inscripcion:::::::::: -->
	<form class="form-inline" role="form" action="control.php" 
	 enctype =  "multipart/form-data" method="post">
	    
	    <div class="form-group">    
		   <label class="sr-only"><?php echo $data["apellido"]?></label>
		  <input type="text" class="form-control" name="apellido" 
		  placeholder="<?php echo $data["apellido"]?>" required  />
		 </div>
	    
		<div class="form-group">    
		   <label class="sr-only"><?php echo $data["nombre"]?></label>
		  <input type="text" class="form-control" name="nombre" placeholder="<?php echo $data["nombre"]?>" required/>
		 </div>	 
		
		<div class="form-group">
          <label class="sr-only "><?php echo $data["tipoDoc"]?></label>          
          <select class="form-control" name="tipoDoc">
            <option>DNI</option>
            <option>CC</option>
            <option>CI</option>
            <option>CIC</option>
            <option>CIE</option>
            <option>LC</option>
            <option>LE</option>
            <option>Pasaporte</option>
            <option>RG</option>
            <option>VISA</option>
            <option>Otro</option>
         </select>
        </div>    
        
        
        <div class="form-group">
            <label class="sr-only">(*)<?php echo $data["nroDoc"]?></label>          
            <input type="text" class="form-control" name="nroDoc" 
            placeholder="<?php echo $data["nroDoc"]?>" required/>
         </div>

         <div class="form-group">        
          <label class="sr-only col-lg-3 col-md-4"><?php echo $data["pais"]?></label>
          
            <select class="form-control" name="pais">
                <option value="Afghanistan">Afghanistan</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina" selected="selected">Argentina</option><option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option><option value="Australia">Australia</option>
                <option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia">Bolivia</option><option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option><option value="Botswana">Botswana</option><option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option><option value="British Indian Ocean Territory">British Indian Ocean Territory</option><option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Canary Islands">Canary Islands</option><option value="Cape Verde">Cape Verde</option><option value="Cayman Islands">Cayman Islands</option><option value="Central African Republic">Central African Republic</option><option value="Chad">Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island">Christmas Island</option><option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option><option value="Colombia">Colombia</option><option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="Cook Islands">Cook Islands</option><option value="Costa Rica">Costa Rica</option><option value="Cote D Ivoire">Cote D Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Cyprus">Cyprus</option><option value="Czech Republic">Czech Republic</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican Republic">Dominican Republic</option><option value="East Timor">East Timor</option><option value="East Timor">East Timor</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option><option value="Faroe Islands">Faroe Islands</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="France, Metropolitan">France, Metropolitan</option><option value="French Guiana">French Guiana</option><option value="French Polynesia">French Polynesia</option><option value="French Southern Territories">French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guinea">Guinea</option><option value="Guinea-bissau">Guinea-bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jersey">Jersey</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="Korea, Democratic People s Republic of">Korea, Democratic People s Republic of</option><option value="Korea, Republic of">Korea, Republic of</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People s Democratic Republic">Lao People s Democratic Republic</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macau">Macau</option><option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Marshall Islands">Marshall Islands</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="Micronesia, Federated States of">Micronesia, Federated States of</option><option value="Moldova, Republic of">Moldova, Republic of</option><option value="Monaco">Monaco</option><option value="Mongolia">Mongolia</option><option value="Montenegro">Montenegro</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="Netherlands Antilles">Netherlands Antilles</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palau">Palau</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Qatar">Qatar</option><option value="Reunion">Reunion</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="Rwanda">Rwanda</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="Samoa">Samoa</option><option value="San Marino">San Marino</option><option value="Sao Tome and Principe">Sao Tome and Principe</option><option value="Saudi Arabia">Saudi Arabia</option><option value="Senegal">Senegal</option><option value="Serbia">Serbia</option><option value="Seychelles">Seychelles</option><option value="Sierra Leone">Sierra Leone</option><option value="Singapore">Singapore</option><option value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="St. Barthelemy">St. Barthelemy</option><option value="St. Eustatius">St. Eustatius</option><option value="St. Helena">St. Helena</option><option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option><option value="Sudan">Sudan</option><option value="Suriname">Suriname</option><option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic">Syrian Arab Republic</option><option value="Taiwan">Taiwan</option><option value="Tajikistan">Tajikistan</option><option value="Tanzania, United Republic of">Tanzania, United Republic of</option><option value="Thailand">Thailand</option><option value="The Democratic Republic of Congo">The Democratic Republic of Congo</option><option value="Togo">Togo</option><option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks and Caicos Islands">Turks and Caicos Islands</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option><option value="Venezuela">Venezuela</option><option value="Viet Nam">Viet Nam</option><option value="Virgin Islands (British)">Virgin Islands (British)</option><option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option><option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option>                    </select>
    
            </select>
            
        </div>
        
        <div class="form-group">
            <label class="sr-only"><?php echo $data["provincia"]?></label>          
            <select class="form-control" name="provincia">
                <option value="1" selected="selected">Buenos Aires</option>
                <option value="2">C.A.B.A.</option>
                <option value="3">Catamarca</option>
                <option value="4">Chaco</option>
                <option value="5">Chubut</option>
                <option value="6">Cordoba</option>
                <option value="7">Corrientes</option>
                <option value="8">Entre Rios</option>
                <option value="9">Formosa</option>
                <option value="10">Jujuy</option>
                <option value="11">La Pampa</option>
                <option value="12">La Rioja</option>
                <option value="13">Mendoza</option>
                <option value="14">Misiones</option>
                <option value="15">Neuquen</option>
                <option value="16">Rio Negro</option>
                <option value="17">Salta</option>
                <option value="18">San Juan</option>
                <option value="19">San Luis</option>
                <option value="20">Santa Cruz</option>
                <option value="21">Santa Fe 1ra. Circ.</option>
                <option value="23">Santiago del Estero</option>
                <option value="24">Tierra del Fuego</option>
                <option value="25">Tucuman</option>
                <option value="22">Santa Fe 2da. Circ</option>
                <option value="0">No Especificado</option>            

            </select>
         </div>
         
         <div class="form-group">        
            <label class="sr-only"><?php echo $data["ciudad"]?></label>            
            <input type="text" class="form-control" name="ciudad" placeholder="<?php echo $data["ciudad"]?>" required/>          
        </div>
        
        <div class="form-group">        
            <label class="sr-only"><?php echo $data["direccion"]?></label>            
            <input type="text" class="form-control" name="direccion" placeholder="<?php echo $data["direccion"]?>" required/>          
        </div>
        
        <div class="form-group">
            <label class="sr-only"><?php echo $data["cp"]?></label>          
            <input type="text" class="form-control" name="cp" placeholder="<?php echo $data["cp"]?>" required/>
         </div>
         
         <div class="form-group">        
            <label class="sr-only"><?php echo $data["telefono"]?></label>          
            <input type="text" class="form-control" name="telefono" placeholder="<?php echo $data["telefono"]?>" required/>
         </div>
       
        
        <div class="form-group">
            <label class="sr-only"><?php echo $data["correo"]?></label>          
            <input type="email" class="form-control" name="correo" placeholder="<?php echo $data["correo"]?>" required/>       
        </div>
        
        <hr />
        
        <div class="form-group" onchange="controlSocio();"> 
          <label><?php echo $data["socio"]?></label>
          <div class="radio-inline">
            <label class="text-muted">
                <input type="radio" name="socioT" id="socioY" value="si" /> Si
            </label>            
          </div>
          <div class="radio-inline">
            <label class="text-muted">
                <input type="radio" name="socioT" id="socioN" value="no" checked /> No
            </label>            
          </div>
         </div>
         
         <div class="form-group hidden" id="entidadSocio" name="entidadSocio">
         	<label class="sr-only"><?php echo $data["entidad"]?></label>         	
         	<?php $socios = new Socios();
         		  echo $socios->selectSocio()?>
         </div>
        
        <div class="form-group">
            <label> <?php echo $data["formasP"]?></label>
            <select class="form-control" id="formasP" name="formasP" 
            onchange="control();" <?php echo $gpl["estado"]?>>
                <option value="efectivo" <?php echo " ".$gpl["efectivo"].">".$data["efectivo"]?></option>
                <option value="deposito"<?php echo " ".$gpl["deposito"].">".$data["deposito"]?></option>
                <option value="tarjeta"<?php echo " ".$gpl["tarjeta"].">".$data["tarjeta"]?></option>
            </select>
        </div>
        
        <div class="form-group hidden" id="transaccion">
         	<label class="sr-only" id="transaccion"><?php echo $data["transaccion"]?></label>
         	<input type="text" class="form-control" name="transaccion" placeholder="<?php echo $data["transaccion"]?>" />
         </div>
         
         <div class="form-group hidden" id="pagosTarjetas">
            <label> <?php echo $data["tarjetaP"]?></label>
            <select class="form-control" id="tarjetaP" name="tarjetaP">
                <option value="1">1</option>
                <!--<option value="2">2</option>
                <option value="3">3</option>
                <option value="3">4</option>
                <option value="3">5</option>
                <option value="3">6</option>
                <option value="3">7</option>
                <option value="3">8</option>
                <option value="3">9</option>
                <option value="3">10</option>
                <option value="3">11</option>
                <option value="3">12</option>
                <option value="3">18</option>
                <option value="3">24</option> -->                 
            </select>
        </div>
        
        <div class="form-group">
         	<label class="sr-only"><?php echo $data["importe"]?></label>
         	<input type="text" class="form-control" name="importe" 
         	placeholder="<?php echo $data["importe"]?>" 
         	value='<?php echo $gpl["importe"]."' ".$gpl["estado"] ?> required/>
         </div>
         
         <div class="form-group hidden">
         	<input type="number" class="form-control" name="pago" value="<?php $gpl["pago"] ?>"/>
         </div>
        
        <div class="form-group" onchange="controlGrupo();"> 
        	<fieldset <?php echo $gpl["estado"] ?>>
          <label><?php echo $data["incripcionG"]?></label>
          <div class="radio-inline">
            <label class="text-muted">
                <input type="radio" name="incripcionG" id="incripcionGrupalY" value="si" <?php echo $gpl["chek1"] ?>>
                Si
            </label>            
          </div>
          <div class="radio-inline">
            <label class="text-muted">
                <input type="radio" name="incripcionG" id="incripcionGrupalN" value="no" <?php echo $gpl["chek2"] ?>> 
                No
            </label>            
          </div>
          </fieldset>
         </div>
         
         <div class="form-group hidden" id="cantPersonas">
         	<label class="sr-only"><?php echo $data["cant"]?></label>
         	<input type="number" class="form-control" name="cant" value="<?php  if($gpl["personas"]!="")
                                                                                    {
                                                                                        echo $gpl["personas"];
                                                                                    }
                                                                                    else
                                                                                   {
	                                                                                   echo "1";
                                                                                    }
                                                                                     ?>"
         	placeholder="<?php echo $data["cant"]."\" ".$gpl["estado"] ?>/>
         </div>      
       
         <!-- **********************Ponencias *************************-->    
        <div class="form-group">
            <label> <?php echo $data["categoria"]?></label>
            <select class="form-control" name="categoria">
                <option value="autor"><?php echo $data["autor"]?></option>
                <option value="coautor"><?php echo $data["coautor"]?></option>
                <option value="participante"><?php echo $data["participante"]?></option>
                <option value="conferencista"><?php echo $data["conferencista"]?></option>
                <option value="estudiante"><?php echo $data["estudiante"]?></option>
                <option value="expositor"><?php echo $data["expositor"]?></option>
            </select>
            <hr />            
         
        	<label class="sr-only"> <?php echo $data["ponencia1"]?> </label>
        	<input type="text" class="form-control" name="ponencia1"
        	 placeholder="<?php echo $data["ponencia1"]?>" />     	       	 
        	 
        	 
        	 <div class="text-center">
        	  <p><b>EJE 1</b></p>
        	<?php  $socios = new Ejes();
                   $socios->add = "1";
                  if($leng == "port")
                  {
                      $socios->muestra = "port";
                  }
                  echo $socios->selectEjes()?>
             </div>
             
             <div >
                   <p><b>Contenido 1</b></p>
              <?php  
                   $cont = new Contenidos;
                   $cont->add = "1";                   
                  /*if($leng == "port")
                  {
                      $cont->muestra = "port";
                  }*/
                  echo $cont->selectEjes();
              ?>   
             
        	   <input type="file" class="form-control" id="ponencia1F" name="ponencia1F"/>
        	   <p class="text-muted"><small>Formatos Permitidos: *.doc o *.docx</small></p>
            </div>
        </div>
        
        <div class="form-group">
        	<label class="sr-only"> <?php echo $data["ponencia2"]?> </label>
        	<input type="text" class="form-control" name="ponencia2" 
        	placeholder="<?php echo $data["ponencia2"]?>"/>
        	
        	<div class="text-center">
        	    <p><b>EJE 2</b></p>
        	<?php $socios = new Ejes(); 
                  $socios->add = "2";
                  if($leng == "port")
                  {
                      $socios->muestra = "port";
                  }                  
                  echo $socios->selectEjes()?>
            </div>
            
            <div >
                   <p><b>Contenido 2</b></p>
              <?php  
                   $cont = new Contenidos;
                   $cont->add = "2";                   
                  /*if($leng == "port")
                  {
                      $cont->muestra = "port";
                  }*/
                  echo $cont->selectEjes();
              ?>   
             
        	   <input type="file" class="form-control" id="ponencia2F" name="ponencia2F"/>
        	   <p class="text-muted"><small>Formatos Permitidos: *.doc o *.docx</small></p>
        	</div>
        </div>     
        
        <div class="form-group">
            <label>Verificar el codigo</label>
            <a href="javascript: actucap();" class="text-primary">Actualizar</a>
            <img src="./lib/captcha.php" class="img-rounded" id="imgcaptcha"/>    
            <input type="text"class="form-control" name="captcha" required/>
            <p class="text-muted"><small>Completar los datos de la imagen</small></p>
        </div>
        
        <div class="hidden">
        	<input type="hidden" class="form-control" name="leng" value="<?php echo $leng?>" />
        </div>
        
        <br />
        <div class="text-center">        
            <button type="submit" class="btn btn-info" name="enviar" id="enviar">Enviar</button>
		    <button type="reset" class="btn btn-default">Cancelar</button>		
		</div>
	</form>
	<div class="text-center">
	   <p class="text-muted">
	       <small>
	           Sistema de inscripcion desarrollado por 
	            <a href="http://infrasoft.com.ar"> 
	                Infrasoft - Servicios Informaticos. http://infrasoft.com.ar
	            </a>	  
	           - © Derechos reservados
	      </small>
	     </p>
	 </div>  
	</div>
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="js/responsive.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/form.js"></script></body>
</html>